//---------------------------------------------------------------------------
#ifndef operating_systemH
#define operating_systemH
//---------------------------------------------------------------------------
#include <wtypes.h>
#include <string>
//---------------------------------------------------------------------------

namespace public_nbs {

class Operating_system
{
public:
	enum Version_type { W95, W98_ME, WNT, W2000, OTHER };
	Version_type version() const { return ver; }
	bool windows_9x() const { return win9x; }
	bool is_2k_or_later() const { return winnt; }
	bool is_xp_or_later() const { return winxp || vista || win7; }
	bool is_vista_or_later() const { return vista || win7; }
	bool is_win7_or_later() const { return win7; }
	bool is_64_bit() const { return bit_64; }
	std::wstring process_current_directory() const;
	std::wstring get_last_error() const;
	std::wstring get_error_message( HRESULT code ) const;

	static Operating_system& singlet();

private:
	Operating_system();
	Operating_system( const Operating_system& ); // unimplemented
	Operating_system& operator=( const Operating_system& ); // unimplemented
	Version_type ver;
	bool win9x;
	bool winnt;
	bool winxp;
	bool vista;
    bool win7;
	bool bit_64;
};

}; // namespace nbs

//---------------------------------------------------------------------------
#endif

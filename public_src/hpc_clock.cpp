//---------------------------------------------------------------------------
#include "hpc_clock.h"
#include "string_utilities.h"
#include <memory>
//---------------------------------------------------------------------------
using namespace public_nbs;
//---------------------------------------------------------------------------
//
// class HPC_clock
//
//--------------------------------------------------------------------------- 
		
HPC_clock::Overflow::Overflow
( 
	LONGLONG min, 
	LONGLONG current, 
	LONGLONG start, 
	const wchar_t* method 
)
	: 
	Exception( report( min, current, start, method ) ), 
	min_( min ), 
	current_( current ), 
	method_( method ), 
	start_( start ) 
{
}

//---------------------------------------------------------------------------

std::wstring HPC_clock::Overflow::report()
{
	return report( min_, current_, start_, method_.c_str() );
}
	
//---------------------------------------------------------------------------

std::wstring HPC_clock::Overflow::report
( 
	sint64 min, 
	sint64 current, 
	sint64 start, 
	const std::wstring& method 
)
{
	std::wstring message = L"There was a clock overflow:\n";
	message = message + L"min: " + to_wstr( min ) + L"\n";
	message = message + L"current: " + to_wstr( current ) + L"\n";
	message = message + L"start: " + to_wstr( start ) + L"\n";
	message = message + L"method: " + method + L"\n";
	message += L"Please report this data to NBS.";
	return message;
}

//---------------------------------------------------------------------------

HPC_clock::HPC_clock()
{
	Large_int freq;
	if (!QueryPerformanceFrequency( (LARGE_INTEGER*)&freq )) 
	{
		throw Exception( L"QueryPerformanceFrequency failed" );
	}
	counts_per_second_ = freq.QuadPart;
	QueryPerformanceCounter( (LARGE_INTEGER*)&freq );
	zero_count_ = min_count_ = freq.QuadPart;
}

//---------------------------------------------------------------------------

Time_micro HPC_clock::time() const
{
	Large_int freq;
	QueryPerformanceCounter( (LARGE_INTEGER*)&freq );
	if (freq.QuadPart < min_count_)
	{
		throw Overflow( min_count_, freq.QuadPart, zero_count_, L"time" );
	}
	sint64 temp = freq.QuadPart - zero_count_;
	temp = (temp / counts_per_second_) * MICROSECONDS_PER_SECOND + 
		(temp % counts_per_second_) * MICROSECONDS_PER_SECOND / counts_per_second_;
	return Time_micro::make_micros( temp );
}

//---------------------------------------------------------------------------

void HPC_clock::set( Time_micro t )
{
	Large_int freq;
	QueryPerformanceCounter( (LARGE_INTEGER*)&freq );
	zero_count_ = freq.QuadPart - t.microseconds() * counts_per_second_ / MICROSECONDS_PER_SECOND;
}

//---------------------------------------------------------------------------




//---------------------------------------------------------------------------
#ifndef numerical_utilitiesH
#define numerical_utilitiesH
//---------------------------------------------------------------------------
#include <algorithm>
#include <iterator>
#include <vector>
#include "types.h"
#include "exception.h"
//---------------------------------------------------------------------------

namespace public_nbs {

/// This is a fixed size queue
template <class T>
class Last_N
{
public:
	Last_N( unsigned int size, T initial_value ) 
		: 
		values_( size, initial_value ), 
		cursor_( 0 ) 
	{
		if (size == 0)
		{
			throw Exception( L"Last_N size must not be zero" );
		}
	}

	/// Adds a new value, dropping the oldest value
	void add( T value )
	{
		values_[cursor_] = value;
		if (++cursor_ == values_.size())
		{
			cursor_ = 0;
		}
	}

	bool contains( T value ) const
	{
		return std::find( values_.begin(), values_.end(), value ) != values_.end();
	}

private:
	std::vector<T> values_;
	unsigned int cursor_;
};

template <class Container,class Element>
void remove_element( Container& c, Element el )
{
	Container::iterator e = std::remove( c.begin(), c.end(), el );
	c.resize( e - c.begin() );
}

template <class Int_type,class Float_type>
Int_type round( Float_type value )
{
	if (value >= 0)
	{
		return static_cast<Int_type>( value + 0.5 );
	}
	return static_cast<Int_type>( value - 0.5 );
}

template <class Iter1, class Iter2, class T>
bool compare( Iter1 i1, Iter2 i2, T terminator )
{
	for ( ; *i1 != terminator && *i2 != terminator; ++i1, ++i2)
	{
		if (*i1 != *i2)
		{
			return false;
		}
	}
	return (*i1 == terminator) && (*i2 == terminator);
}

template <class Iter1, class Iter2>
bool compare( Iter1 begin1, Iter1 end1, Iter2 begin2, Iter2 end2 )
{
	for ( ; begin1 != end1 && begin2 != end2; ++begin1, ++begin2)
	{
		if (*begin1 != *begin2)
		{
			return false;
		}
	}
	return (begin1 == end1) && (begin2 == end2);
}

template <class Iter1, class Iter2>
bool starts_with( Iter1 arg_begin, Iter1 arg_end, Iter2 prefix_begin, Iter2 prefix_end )
{
	for ( ; arg_begin != arg_end && prefix_begin != prefix_end; ++arg_begin, ++prefix_begin)
	{
		if (*arg_begin != *prefix_begin)
		{
			return false;
		}
	}
	return (prefix_begin == prefix_end);
}

template <class Cont1, class Cont2>
bool starts_with( const Cont1& arg, const Cont2& prefix )
{
	return starts_with( arg.begin(), arg.end(), prefix.begin(), prefix.end() );
}

template <class T>
void flip_sign_bit( T& t );

template <>
inline void flip_sign_bit<sint8>( sint8& i )
{
	i ^= 0x80;
}

template <>
inline void flip_sign_bit<sint16>( sint16& i )
{
	i ^= 0x8000;
}

template <>
inline void flip_sign_bit<Sint24>( Sint24& i )
{
	i.flip_sign_bit();
}

template <>
inline void flip_sign_bit<sint32>( sint32& i )
{
	i ^= 0x80000000;
}


template <class To_type, class From_type, bool to_signed, bool from_signed>
struct Upsample;

template <class To_type, class From_type>
struct Upsample<To_type,From_type,false,false>
{
	static void upsample( To_type& to, const From_type& from )
	{
		STATIC_CHECK( sizeof(To_type) > sizeof(From_type), upsample_wrong_sizes )
		STATIC_CHECK( !std::numeric_limits<To_type>::is_signed && !std::numeric_limits<From_type>::is_signed, upsample_wrong_signs )
		const uint32 shift = (sizeof(To_type) - sizeof(From_type)) * 8;
		to = static_cast<To_type>( static_cast<uint32>( from ) << shift );
	}
};

template <class To_type, class From_type>
struct Upsample<To_type,From_type,true,true>
{
	static void upsample( To_type& to, const From_type& from )
	{
		STATIC_CHECK( sizeof(To_type) > sizeof(From_type), upsample_wrong_sizes )
		STATIC_CHECK( std::numeric_limits<To_type>::is_signed && std::numeric_limits<From_type>::is_signed, upsample_wrong_signs )
		const uint32 shift = (sizeof(To_type) - sizeof(From_type)) * 8;
		to = static_cast<To_type>( static_cast<uint32>( from ) << shift );
	}
};

template <class To_type, class From_type>
struct Upsample<To_type,From_type,true,false>
{
	static void upsample( To_type& to, const From_type& from )
	{
		STATIC_CHECK( sizeof(To_type) > sizeof(From_type), upsample_wrong_sizes )
		STATIC_CHECK( std::numeric_limits<To_type>::is_signed && !std::numeric_limits<From_type>::is_signed, upsample_wrong_signs )
		const uint32 shift = (sizeof(To_type) - sizeof(From_type)) * 8;
		to = static_cast<To_type>( static_cast<uint32>( from ) << shift );
		flip_sign_bit( to );
	}
};

/// \warning You should call this function without specifying the template arguments
/// so that the same type version can be used if the types are the same
template <class From_type, class To_type>
void upsample( To_type& to, const From_type& from )
{
	STATIC_CHECK( sizeof(To_type) > sizeof(From_type), upsample_wrong_sizes )
	Upsample<To_type,From_type,
			std::numeric_limits<To_type>::is_signed,
			std::numeric_limits<From_type>::is_signed>::upsample( to, from );
}

template <class T>
void upsample( T& to, const T& from ) 
{ 
	to = from; 
}

template <class Iter1,class Iter2>
void upsample( Iter1 begin, Iter1 end, Iter2 to )
{
	for ( ; begin != end; ++begin,++to)
	{
		upsample( *to, *begin );
	}
}

/// Iterator to access data stored in an array, but whose values are seperated in memory
/// by some given number of bytes. This is most often used for multiplexed sound or image data
template <class T>
class Interleaved_data_iterator : public std::iterator<std::random_access_iterator_tag,T>
{
public:
	Interleaved_data_iterator() {}

	Interleaved_data_iterator( void* data, unsigned int block_size_bytes )
		:
		data_( static_cast<uint8*>( data ) ),
		block_size_bytes_( block_size_bytes )
	{
	}

	T& operator*() { return *reinterpret_cast<T*>( data_ ); }

	T* operator->() { return reinterpret_cast<T*>( data_ ); }

	T& operator[]( size_t index ) { return *reinterpret_cast<T*>( data_ + block_size_bytes_ * index); }

	bool operator==( const Interleaved_data_iterator& rhs ) const { return data_ == rhs.data_; }

	bool operator!=( const Interleaved_data_iterator& rhs ) const { return !(*this == rhs); }
	
	bool operator<( const Interleaved_data_iterator& rhs ) const { return data_ < rhs.data_; }
	
	bool operator>( const Interleaved_data_iterator& rhs ) const { return data_ > rhs.data_; }
	
	bool operator<=( const Interleaved_data_iterator& rhs ) const { return data_ <= rhs.data_; }
	
	bool operator>=( const Interleaved_data_iterator& rhs ) const { return data_ >= rhs.data_; }

	Interleaved_data_iterator& operator+=( size_t rhs ) { data_ += block_size_bytes_ * rhs; return *this; }

	Interleaved_data_iterator& operator-=( size_t rhs ) { data_ -= block_size_bytes_ * rhs; return *this; }

	Interleaved_data_iterator& operator++() { data_ += block_size_bytes_; return *this; }

	const Interleaved_data_iterator operator++(int) { Interleaved_data_iterator temp( *this ); data_ += block_size_bytes_; return temp; }

	Interleaved_data_iterator& operator--() { data_ -= block_size_bytes_; return *this; }

	const Interleaved_data_iterator operator--(int) { Interleaved_data_iterator temp( *this ); data_ -= block_size_bytes_; return temp; }

	size_t operator-( const Interleaved_data_iterator<T>& rhs ) const { return (data_ - rhs.data_) / block_size_bytes_; }

private:
	uint8* data_;
	size_t block_size_bytes_;
};

template <class T>
Interleaved_data_iterator<T> operator+( const Interleaved_data_iterator<T>& lhs, size_t rhs )
{
	Interleaved_data_iterator<T> temp( lhs );
	temp += rhs;
	return temp;
}

template <class T>
Interleaved_data_iterator<T> operator+( size_t lhs, const Interleaved_data_iterator<T>& rhs )
{
	Interleaved_data_iterator<T> temp( rhs );
	temp += lhs;
	return temp;
}

template <class T>
Interleaved_data_iterator<T> operator-( const Interleaved_data_iterator<T>& lhs, size_t rhs )
{
	Interleaved_data_iterator<T> temp( lhs );
	temp -= rhs;
	return temp;
}

/// In VS2008, conversion from double to sint32 clips instead of truncates.
/// We obtain original behavior by first converting to sint64
inline sint32 truncate_to_sint32( double d )
{
	return sint32( sint64( d ) );
}


} // namespace public_nbs

//---------------------------------------------------------------------------
#endif
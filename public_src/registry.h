//---------------------------------------------------------------------------
#ifndef registryH
#define registryH
//---------------------------------------------------------------------------
#include "types.h"
#include "nbs/ptr.h"
#include <string>
#include <vector>
#include "windows.h"
//---------------------------------------------------------------------------

namespace public_nbs {

class Registry_key
{
public:
	Registry_key( HKEY handle ) : handle_( handle ) {}
	~Registry_key() { RegCloseKey( handle_ ); }

	/// \brief Gets a list of the direct subkey names
	/// 
	/// \param keys Vector into which the names are pushed. The 
	/// vector is not cleared first.
	///
	/// \throw Exception on failure
	void get_subkeys( std::vector<std::wstring>& keys );

	/// \brief Gets a list of the values in this key
	///
	/// \param values Vector into which the names are pushed. The
	/// vector is not cleared first.
	///
	/// \throw Exception on failure
	void get_values( std::vector<std::wstring>& values );
	
	/// \brief opens or creates a subkey
	///
	/// \param name The path of the key relative to the current key.
	/// The key does not have to be an immediate child key - even if
	/// it is created.
	///
	/// \param write Indicates if the key needs write permissions
	///
	/// \param create If the key does not exist, indicates if the
	/// key is created. 
	///
	/// \throw Exception on failure
	Ptr<Registry_key>::Shared_ptr open_subkey( const std::wstring& name, bool write = true, bool create = true );

	/// \brief Indicates if a key exists
	///
	/// \param name The path of the key relative to the current key
	///
	/// \return True if the key exists
	bool subkey_exists( const std::wstring& name );

	/// \brief deletes a key (and all values and subkeys)
	///
	/// \param name The path of the key relative to the current key
	///
	/// \throw Exception if the key was deleted or did not exist, 
	/// or another error
	void delete_subkey( const std::wstring& name );

	/// \brief deletes the parameter and value
	///
	/// \param name The name of the parameter
	///
	/// \throw Exception if the parameter was deleted, an error if
	/// the parameter does not exist, or another error
	void delete_value( const std::wstring& name );

	/// \brief Sets the parameter value to a string, creating it if necessary
	///
	/// \param name The name of the parameter
	/// 
	/// \param value The new value of the parameter
	///
	/// \throw Exception on failure
	///
	/// If the parameter already exists with another type, the type is
	/// changed to REG_SZ
	void set_value( const std::wstring& name, const std::wstring& value );

	/// \brief Sets the parameter value to an integer, creating it if necessary
	///
	/// \param name The name of the parameter
	/// 
	/// \param value The new value of the parameter
	///
	/// \throw Exception on failure
	///
	/// If the parameter already exists with another type, the type is
	/// changed to REG_DWORD
	void set_value( const std::wstring& name, uint32 value );

	/// \brief Sets the parameter value to the binary data, creating it if necessary
	///
	/// \param name The name of the parameter
	///
	/// \param value The new value of the parameter
	///
	/// \throw Exception on failure
	///
	/// If the parameter already exists with another type, the type is
	/// change to REG_BINARY
	void set_value( const std::wstring& name, double value );

	enum Value_type { NO_VALUE, STRING, UINT32, UINT64, BINARY, NONE, OTHER };
	/// \brief Gets the data type of a parameter
	///
	/// \param name The name of the parameter
	///
	/// \return The data type of the parameter, or NO_VALUE if no parameter
	/// by that name exists in the key
	Value_type get_type( const std::wstring& name );

	/// \brief Gets the value of an UINT32 parameter
	///
	/// \param name The name of the parameter
	///
	/// \return The value of the parameter, or 0 if the parameter
	/// does not exist or is not of type UINT32
	uint32 get_int32( const std::wstring& name );

	/// \brief Gets the value of an string parameter
	///
	/// \param name The name of the parameter
	///
	/// \return The value of the parameter, or an empty string
	/// if the parameter does not exist or is not a string
	std::wstring get_string( const std::wstring& name );

	/// \brief Gets the value of an double parameter
	///
	/// \param name The name of the parameter
	///
	/// \return The value of the parameter, or 0 if the parameter
	/// does not exist or is not of type REG_BINARY
	double get_double( const std::wstring& name );

private:
	Registry_key( const Registry_key& );
	Registry_key& operator=( const Registry_key& ref );

	HKEY handle_;
};

/// \brief Wraps the Windows registry
class Registry
{
public:
	static Registry& singlet();

	enum Root_type { CLASSES, CURRENT_USER, LOCAL_MACHINE, USERS };

	/// \brief opens or creates a key
	///
	/// \param key Pointer set to the new Registry_key object
	/// 
	/// \param root The root key of the opened key
	///
	/// \param name The path of the key relative to the root key
	///
	/// \param write Indicates if the key needs write permissions
	///
	/// \param create If the key does not exist, indicates if the
	/// key is created. 
	///
	/// \throw Exception on failure
	Ptr<Registry_key>::Shared_ptr open_key( Root_type root, 
		const std::wstring& name, bool write = true, bool create = true );

	/// \brief deletes a key (and all values and subkeys)
	///
	/// \param root The root key of the deleted key
	///
	/// \param name The path of the key relative to the root key
	///
	/// \throw Exception on failure
	void delete_key( Root_type root, const std::wstring& name );
	
	/// \brief Indicates if a key exists
	///
	/// \param root The root key of the specified key
	///
	/// \param name The path of the key relative to the root key
	///
	/// \return True if the key exists
	bool key_exists( Root_type root, const std::wstring& name );

private:
	Registry() {}
	Registry( const Registry& );
	Registry& operator=( const Registry& );

	HKEY root_key( Root_type type );
};

}; // end namespace public_nbs

//---------------------------------------------------------------------------
#endif
 
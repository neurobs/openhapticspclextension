//---------------------------------------------------------------------------
#ifndef typesH
#define typesH
//---------------------------------------------------------------------------
#include "loki\static_check.h"
#include <limits>
//---------------------------------------------------------------------------

namespace public_nbs {

// Quick conversion chart:
// BYTE = uint8
// WORD = uint16
// DWORD = uint32
// LONGLONG = sint64
// ULONGLONG = uint64
// LARGE_INTEGER = Large_int

typedef unsigned char      uint8;    // 8 -bit unsigned
typedef char               sint8;   // 8 -bit   signed

typedef unsigned short int uint16;    // 16-bit unsigned
typedef short int          sint16;   // 16-bit   signed

typedef unsigned long int  uint32;   // 32-bit unsigned
typedef long int           sint32;  // 32-bit   signed

typedef unsigned __int64   uint64;   // 64-bit   unsigned
typedef __int64            sint64;   // 64-bit   signed

typedef float				sfloat32; // 32 bit signed
typedef double				sfloat64; // 64-bit signed

const int LITTLE_ENDIAN_PLATFORM = 1;

#pragma pack(push)
#pragma pack(1)
template <int> struct Int32_storage_base
{
	uint8 byte3;
	uint8 byte2;
	uint8 byte1;
	uint8 byte0;
};
#pragma pack(pop)

#pragma pack(push)
#pragma pack(1)
template <> struct Int32_storage_base<1>
{
	uint8 byte0;
	uint8 byte1;
	uint8 byte2;
	uint8 byte3;
};
#pragma pack(pop)

typedef Int32_storage_base<LITTLE_ENDIAN_PLATFORM> Int32_storage;

typedef union _Large_int {
    struct {
        uint32 LowPart;
        sint32 HighPart;
    };
    struct {
        uint32 LowPart;
        sint32 HighPart;
    } u;
    sint64 QuadPart;
} Large_int;

STATIC_CHECK( sizeof(uint8) == 1, uint8_wrong_size )
STATIC_CHECK( !std::numeric_limits<uint8>::is_signed, uint8_wrong_sign )

STATIC_CHECK( sizeof(sint8) == 1, sint8_wrong_size )
STATIC_CHECK( std::numeric_limits<sint8>::is_signed, sint8_wrong_sign )

STATIC_CHECK( sizeof(uint16) == 2, uint16_wrong_size )
STATIC_CHECK( !std::numeric_limits<uint16>::is_signed, uint16_wrong_sign )

STATIC_CHECK( sizeof(sint16) == 2, sint16_wrong_size )
STATIC_CHECK( std::numeric_limits<sint16>::is_signed, sint16_wrong_sign )

STATIC_CHECK( sizeof(uint32) == 4, uint32_wrong_size )
STATIC_CHECK( !std::numeric_limits<uint32>::is_signed, uint32_wrong_sign )

STATIC_CHECK( sizeof(sint32) == 4, sint32_wrong_size )
STATIC_CHECK( std::numeric_limits<sint32>::is_signed, sint32_wrong_sign )

//STATIC_CHECK( std::numeric_limits<Int64>::is_signed, Int64_wrong_sign )  
STATIC_CHECK( sizeof(sint64) == 8, Int64_wrong_size )

STATIC_CHECK( sizeof(sfloat32) == 4, sfloat32_wrong_size )
STATIC_CHECK( std::numeric_limits<sfloat32>::is_signed, sfloat32_wrong_sign )

STATIC_CHECK( sizeof(sfloat64) == 8, sfloat64_wrong_size )
STATIC_CHECK( std::numeric_limits<sfloat32>::is_signed, sfloat64_wrong_sign )

#pragma pack(push)
#pragma pack(1)
/// This class is used to do processing on 24 bit integers that are
/// packed together in a buffer. The #pragma pack must be around this class
/// to ensure that it's byte size is 3.
class Sint24
{
public:
	Sint24() {}

	Sint24( const Sint24& rhs ) { memcpy( data, rhs.data, 3 ); }

	explicit Sint24( sint32 rhs ) { memcpy( data, &rhs, 3 ); }

	explicit Sint24( uint32 rhs ) { memcpy( data, &rhs, 3 ); }

	explicit Sint24( sint64 rhs ) { memcpy( data, &rhs, 3 ); }

	explicit Sint24( uint64 rhs ) { memcpy( data, &rhs, 3 ); }

	explicit Sint24( double rhs ) { sint32 temp = static_cast<sint32>( rhs ); memcpy( data, &temp, 3 ); }

	Sint24( uint8 rhs )
	{
		STATIC_CHECK( LITTLE_ENDIAN_PLATFORM, Sint24_sint8_not_implemented )
		data[0] = rhs;
		data[1] = data[2] = 0;
	}

	Sint24( sint8 rhs )
	{
		STATIC_CHECK( LITTLE_ENDIAN_PLATFORM, Sint24_sint8_not_implemented )
		data[0] = rhs;
		if (rhs >= 0)
		{
			data[1] = data[2] = 0;
		}
		else
		{
			data[1] = data[2] = 0xFF;
		}
	}

	Sint24( uint16 rhs )
	{
		STATIC_CHECK( LITTLE_ENDIAN_PLATFORM, Sint24_uint16_not_implemented )
		*reinterpret_cast<uint16*>( this ) = rhs;
		data[2] = 0;
	}

	Sint24( sint16 rhs )
	{
		STATIC_CHECK( LITTLE_ENDIAN_PLATFORM, Sint24_sint16_not_implemented )
		*reinterpret_cast<sint16*>( this ) = rhs;
		if (rhs >= 0)
		{
			data[2] = 0;
		}
		else
		{
			data[2] = 0xFF;
		}
	}

	Sint24& operator=( Sint24 rhs ) { memcpy( data, rhs.data, 3 ); return *this; }

	operator sint32() const
	{
		STATIC_CHECK( LITTLE_ENDIAN_PLATFORM, Sint24_sint32_not_implemented )
		sint32 rval( 0 );
		memcpy( &rval, data, 3 );
		if (data[2] >= 0x80)
		{
			reinterpret_cast<Int32_storage*>( &rval )->byte3 = 0xFF;
		}
		return rval;
	}

	operator uint32() const
	{
		return operator sint32();
	}

	operator sint64() const
	{
		return operator sint32();
	}

	operator uint64() const
	{
		return operator sint32();
	}

	operator double() const
	{
		return operator sint32();
	}

	void flip_sign_bit() { data[2] ^= 0x80; }

	bool operator==( Sint24 rhs ) const 
	{
		return data[0] == rhs.data[0] &&
			data[1] == rhs.data[1] &&
			data[2] == rhs.data[2];
	}

	bool operator!=( Sint24 rhs ) const
	{
		return !(*this == rhs );
	}
	
private:
	uint8 data[3];
};
#pragma pack(pop)

STATIC_CHECK( sizeof(Sint24) == 3, Sint24_wrong_size )

} // end namespace public_nbs

//---------------------------------------------------------------------------
#endif
//---------------------------------------------------------------------------
#ifndef ptrH
#define ptrH
//---------------------------------------------------------------------------
#include <memory>
//---------------------------------------------------------------------------

namespace public_nbs {

template <class T>
class Ptr
{
public:
	typedef std::shared_ptr<T> Shared_ptr;
	typedef std::shared_ptr<const T> Const_shared_ptr;

	template <class To>
	static typename Ptr<To>::Shared_ptr dynamic_ptr_cast( const Shared_ptr& p )
	{
		return boost::dynamic_pointer_cast<To,T>( p );
	}

	template <class To>
	static typename Ptr<To>::Const_shared_ptr dynamic_ptr_cast( const Const_shared_ptr& p )
	{
		return boost::dynamic_pointer_cast<To,T>( p );
	}
};

template <class To, class From>
inline typename Ptr<To>::Shared_ptr dynamic_ptr_cast( std::shared_ptr<From> p )
{
	return boost::dynamic_pointer_cast<To,From>( p );
}

template <class To, class From>
inline typename Ptr<To>::Const_shared_ptr dynamic_ptr_cast( std::shared_ptr<const From> p )
{
	return boost::dynamic_pointer_cast<To,From>( p );
}

template <class T>
inline void zero_pointer( std::shared_ptr<T>& p )
{
	p.reset();
}

template <class T>
inline bool is_pointer_null( const std::shared_ptr<T>& p )
{
	return p.get() == 0;
}

template <class T>
inline T* raw_pointer( const std::shared_ptr<T>& p )
{
	return p.get();
}

template <class T>
inline void zero_pointer( T*& p )
{
	p = 0;
}

template <class T>
inline bool is_pointer_null( const T* p )
{
	return p == NULL;
}

template <class T>
inline T* raw_pointer( T* p )
{
	return p;
}

} // end namespace nbs

//---------------------------------------------------------------------------
#endif
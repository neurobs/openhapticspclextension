#pragma once

#include "atlcom.h"
#include "com_ptr.h"
#include "exception.h"

template <class Com_type, class Base_type>
public_nbs::COM_ptr<Com_type> create_com_object( Base_type*& object )
{
	ATL::CComObject<Base_type>* temp = 0;
	if (FAILED(ATL::CComObject<Base_type>::CreateInstance( &temp )) || !temp)
	{
		throw public_nbs::Exception( L"Unable to create local COM class object" );
	}
	temp->AddRef();
	object = temp;
	return public_nbs::COM_ptr<Com_type>( temp );
}

#define COM_METHOD_START \
	HRESULT rval = S_OK; \
	try {

#define COM_METHOD_END( METHOD_NAME )           \
	}                                           \
	catch (public_nbs::Exception& e) {                 \
		last_error_ = e.message();              \
		rval = E_FAIL;                          \
	} catch (...) {                             \
		last_error_ = L"Unknown exception in "; \
		last_error_ += METHOD_NAME;             \
	}                                           \
	return rval;
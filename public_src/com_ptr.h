//---------------------------------------------------------------------------
#ifndef com_ptrH
#define com_ptrH
//---------------------------------------------------------------------------
#include "guiddef.h"
#include "unknwn.h"
#include "com_initializer.h"
#include "string_utilities.h"
#include "exception.h"
//---------------------------------------------------------------------------

namespace public_nbs {

// smart pointer for COM interfaces
// NOTE that constructor with pointer argument and reset do not call AddRef
template<class X>
class COM_ptr
{
public:
	COM_ptr() : ptr( 0 ) {}
	COM_ptr( X* p ) : ptr( p ) {}
	COM_ptr( const COM_ptr<X>& r ) : ptr( r.get() ) { if (ptr) ptr->AddRef(); }
	template <class T>
	COM_ptr( const COM_ptr<T>& r ) : ptr( r.get() ) { if (ptr) ptr->AddRef(); }
	~COM_ptr() { if (ptr) { ptr->Release(); } }
	void operator= (const COM_ptr<X>& r) {
		COM_ptr<X> temp( r );
		swap( temp );
	}
	X& operator* () const { return *ptr; }
	X* operator-> () const { return ptr; }
	X* get() const { return ptr; }
	void reset( X* p ) {
		COM_ptr<X> temp( p );
		swap( temp );
	}                           
	X** assignee() {
		reset( 0 );
		return &ptr;
	}
	bool operator<( const COM_ptr<X>& p ) const { return ptr < p.ptr; }
	bool operator==( const COM_ptr<X>& p ) const { return ptr == p.ptr; }
	bool operator!=( const COM_ptr<X>& p ) const { return ptr != p.ptr; }
	bool operator!() const { return !ptr; }

	static COM_ptr<X> create( REFCLSID class_id, REFIID interface_id,
		LPUNKNOWN aggregate = NULL, DWORD context = CLSCTX_INPROC_SERVER );

private:
	void swap( COM_ptr<X>& rhs ) { std::swap( ptr, rhs.ptr ); }
	X* ptr;
};
    
template<class X>
COM_ptr<X> COM_ptr<X>::create( REFCLSID class_id, REFIID interface_id,
   	LPUNKNOWN aggregate, DWORD context )
{
	X* temp_ptr = 0;
	public_nbs::COM_initializer::singlet();
	HRESULT res = CoCreateInstance( class_id, aggregate, context, interface_id, (void**)&temp_ptr );

	if (res != S_OK)
	{
		std::wstring error;
		switch (res) {
			case REGDB_E_CLASSNOTREG: error = L"Class not registered"; break;
			case E_OUTOFMEMORY: error = L"Out of memory"; break;
			case E_INVALIDARG: error = L"Invalid argument"; break;
			case CLASS_E_NOAGGREGATION: error = L"No aggregation"; break;
			default: error = L"Error code " + to_wstr( res );
		}
		throw Exception( error );
	}

	return COM_ptr<X>( temp_ptr );
}

template <class T>
void check_com_call
( 
	HRESULT res, 
	T* object, 
	const std::wstring& method_name 
)
{
	if (FAILED(res))
	{
		B_str message;
		if (FAILED(object->getLastError( message.assignee() )))
		{
			throw Exception( L"Unknown error calling " + method_name );
		}
		else
		{
			throw Exception( message.str() );
		}
	}
}

template <class T2, class T1>
COM_ptr<T2> query_interface
(
	COM_ptr<T1> i,
	REFIID iid
)
{
	COM_ptr<T2> rval;
	if (FAILED(i->QueryInterface( iid, reinterpret_cast<void**>(rval.assignee()) )))
	{
		throw Exception( L"Queried interface not supported." );
	}
	return rval;
}

} // namespace public_nbs

//---------------------------------------------------------------------------
#endif
 
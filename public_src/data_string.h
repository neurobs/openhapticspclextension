//---------------------------------------------------------------------------
#ifndef data_stringH
#define data_stringH
//---------------------------------------------------------------------------
#include "types.h"
#include "exception.h"
#include <string>
#include <vector>
#include <algorithm>
//---------------------------------------------------------------------------

namespace public_nbs
{

/// Contains arbitrary binary data.
class Data_string : public std::vector<uint8>
{
public:
	Data_string() {}

	explicit Data_string( size_type count ) : std::vector<uint8>( count ) {}

	Data_string( size_type count, const uint8& value ) : std::vector<uint8>( count, value ) {}

	Data_string( const Data_string& rhs ) : std::vector<uint8>( rhs ) {}

	template<class InputIterator>
	Data_string( InputIterator first, InputIterator last ) : std::vector<uint8>( first, last ) {}

	uint8* data() { return &front(); }
	const uint8* data() const { return &front(); }

	template <class T>
	Data_string( const std::basic_string<T>& s )
	{
		resize( s.size() * sizeof(T) );
		memcpy( data(), s.c_str(), size() );
	}
	
	template <class T>
	Data_string( const T* s )
	{
		std::basic_string<T> temp( s );
		resize( temp.size() * sizeof(T) );
		memcpy( data(), temp.c_str(), size() );
	}

	Data_string( const void* c, size_type size_bytes )
	{
		resize( size_bytes );
		memcpy( data(), c, size_bytes );
	}
	
	/// \return Data_string subset from start bytes through start + size_bytes
	///
	/// \throw Exception start + size_bytes > size()
	Data_string subset( size_type start, size_type size_bytes )
	{
		if (start + size_bytes > size()) 
		{
			throw Exception( L"Data_string::subset() called with arguments out of range." );
		}
		return Data_string( begin() + start, begin() + start + size_bytes );
	}

	/// \return the number of whole T's that fit insize the data
	template<class T>
	size_type size_t() const { return size() / sizeof(T); }

	/// \brief Get a casted pointer to any position in the data
	///
	/// \throw Exception pos_bytes + sizeof(T) > size_bytes()
	template<class T>
	T* get( size_type pos_bytes = 0 )
	{
		if (pos_bytes + sizeof(T) > size()) 
		{
			throw Exception( L"Data_string::get() tried to go beyond the end of the buffer." );
		}
		return reinterpret_cast<T*>( data() + pos_bytes );
	}

	/// \brief Get a casted pointer to any position in the data
	///
	/// \throw Exception pos_bytes + sizeof(T) > size()
	template<class T>
	const T* get( size_type pos_bytes = 0 ) const
	{
		if (pos_bytes + sizeof(T) > size()) 
		{
			throw Exception( L"Data_string::get() tried to go beyond the end of the buffer." );
		}
		return reinterpret_cast<const T*>( data() + pos_bytes );
	}

	/// \brief Grab an object from out of the data
	///
	/// \return pos_bytes + sizeof(T)
	template<class T>
	size_type get( T& obj, size_type pos_bytes ) const
	{
		memcpy( &obj, get<T>( pos_bytes ), sizeof(T) );
		return pos_bytes + sizeof(T);
	}

	/// \brief Drop an object into the data, increasing size if necessary
	///
	/// \return pos_bytes + sizeof(T)
	template<class T>
	size_type set( const T& obj, size_type pos_bytes )
	{
		if (pos_bytes + sizeof(T) > size()) 
		{
			resize( pos_bytes + sizeof(T) );
		}
		memcpy( &(*this)[pos_bytes], &obj, sizeof(T) );
		return pos_bytes + sizeof(T);
	}

	/// \brief Adds an object to the end of the data, increasing the size
	///
	/// \return new size
	template <class T>
	size_type append( const T& obj )
	{
		return set( obj, size() );
	}

	/// \brief Whether this contains any nulls, making it dangerous to use as a string
	template<class T>
	bool has_null_t() const
	{
		const T* i = get<T>();
		const T* e = i + size() / sizeof(T);
		return std::find( i, e, 0 ) != e;
	}
	bool has_null() const { return has_null_t<char>(); }
	bool has_wnull() const { return has_null_t<wchar_t>(); }

	/// \brief Return a basic_string.  Careful, there may be nulls inside.
	///
	/// \param throw_on_null Will check for nulls in the string and throw if there are any.
	template<class CharT>
	std::basic_string<CharT> str_t( bool throw_on_null = false ) const
	{
		if (throw_on_null && has_null_t<CharT>()) 
		{
			throw Exception( L"Data_string::str() called on data with nulls in it." );
		}
		return size_t<CharT>() ? std::basic_string<CharT>( get<CharT>(), size_t<CharT>() ) : std::basic_string<CharT>();
	}
	std::string str( bool throw_on_null = false ) const { return str_t<char>(throw_on_null); }
	std::wstring wstr( bool throw_on_null = false ) const { return str_t<wchar_t>(throw_on_null); }

	Data_string& operator+=( const Data_string& rhs ) 
	{ 
		insert( end(), rhs.begin(), rhs.end() ); 
		return *this; 
	}
};

inline bool operator==( const Data_string& lhs, const Data_string& rhs )
{
	return static_cast<std::vector<uint8> >( lhs ) == static_cast<std::vector<uint8> >( rhs );
}

inline bool operator!=( const Data_string& lhs, const Data_string& rhs )
{
	return static_cast<std::vector<uint8> >( lhs ) != static_cast<std::vector<uint8> >( rhs );
}

inline Data_string operator+( const Data_string& lhs, const Data_string& rhs )
{
	Data_string temp( lhs );
	temp += rhs;
	return temp;
}

} // namespace public_nbs

//---------------------------------------------------------------------------
#endif
//---------------------------------------------------------------------------
#ifndef time_microH
#define time_microH
//---------------------------------------------------------------------------
#include "types.h"
#include "exception.h"
//---------------------------------------------------------------------------

#undef max
#undef min

namespace public_nbs {

/// Time value with a precision of 1 microsecond
class Time_micro
{
private:
	typedef sint64 Time_type;
	/// time in microseconds
	Time_type time_;

	Time_micro( Time_type microseconds ) : time_( microseconds ) {}

	static const Time_type MICROS_PER_MILLISECOND = 1000;
	static const Time_type MICROS_PER_SECOND = MICROS_PER_MILLISECOND * 1000;

public:
	/// Time zero
	Time_micro() : time_( 0 ) {}

	static Time_micro make_seconds( sint32 seconds ) { return Time_micro( seconds * MICROS_PER_SECOND ); }

	static Time_micro make_seconds( double seconds ) { return Time_micro( static_cast<Time_type>( seconds * MICROS_PER_SECOND ) ); }

	static Time_micro make_ms( sint32 milliseconds ) { return Time_micro( milliseconds * MICROS_PER_MILLISECOND ); }

	static Time_micro make_ms( double milliseconds ) { return Time_micro( static_cast<Time_type>( milliseconds * MICROS_PER_MILLISECOND ) ); }

	static Time_micro make_micros( sint64 microseconds ) { return Time_micro( microseconds ); }

	static Time_micro maximum() { return Time_micro( std::numeric_limits<Time_type>::max() ); }

	static Time_micro minimum() { return Time_micro( std::numeric_limits<Time_type>::min() ); }

	double ms() const { return static_cast<double>( time_ ) / MICROS_PER_MILLISECOND; }

	double seconds() const { return static_cast<double>( time_ ) / MICROS_PER_SECOND; }

	sint64 microseconds() const { return time_; }

	bool operator<( const Time_micro& rhs ) const
	{ 
		return time_ < rhs.time_; 
	}

	bool operator>( const Time_micro& rhs ) const 
	{ 
		return time_ > rhs.time_; 
	}

	bool operator<=( const Time_micro& rhs ) const 
	{ 
		return time_ <= rhs.time_; 
	}

	bool operator>=( const Time_micro& rhs ) const 
	{ 
		return time_ >= rhs.time_; 
	}

	bool operator==( const Time_micro& rhs ) const 
	{ 
		return time_ == rhs.time_; 
	}

	bool operator!=( const Time_micro& rhs ) const 
	{ 
		return time_ != rhs.time_; 
	}

	const Time_micro operator-() const 
	{ 
		return Time_micro( -time_ ); 
	}

	Time_micro& operator+=( const Time_micro& rhs ) 
	{
		time_ += rhs.time_; return *this;
	}

	Time_micro& operator-=( const Time_micro& rhs ) 
	{
		time_ -= rhs.time_; return *this;
	}

	const Time_micro operator+( const Time_micro& rhs ) const 
	{
		return Time_micro( time_ + rhs.time_ );
	}

	const Time_micro operator-( const Time_micro& rhs ) const 
	{
		return Time_micro( time_ - rhs.time_ );
	}

	const Time_micro operator*( double rhs ) const
	{
		return Time_micro( static_cast<Time_type>( time_ * rhs ) );
	}

	const Time_micro operator/( double rhs ) const
	{
		if (rhs == 0)
		{
			throw Exception( L"Time_micro::operator/ divide by zero" );
		}
		return Time_micro( static_cast<Time_type>( time_ / rhs ) );
	}
};

inline Time_micro operator*( double lhs, const Time_micro& rhs )
{
	return rhs * lhs;
}

} // end namespace public_nbs

//---------------------------------------------------------------------------
#endif
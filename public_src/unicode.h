//---------------------------------------------------------------------------
#ifndef unicodeH
#define unicodeH
//---------------------------------------------------------------------------
#include <string>
#include <tchar.h>
#include "types.h"
#include <vector>
//---------------------------------------------------------------------------

namespace public_nbs {

typedef wchar_t Unicode_char;

typedef std::basic_string<Unicode_char> Unicode_string;

class UTF8_converter
{
public:
	UTF8_converter() : unicode_( 0 ), characters_remaining_( 0 ) {}
	// returns true if the character is valid in the sequence
	bool add( uint8 character )
	{
		bool res = true;
		if (character == 0xFE || character == 0xFF) 
		{
			characters_remaining_ = 0;
			unicode_ = 0;
			res = false;
		} 
		else if (characters_remaining_ == 0) 
		{
			if ((character & 0x80) == 0 )
			{
				unicode_ = character;
			} 
			else if ((character & 0xF0) == 0xF0) 
			{ //we don't take characters longer than 2 bytes
				unicode_ = 0;
				res = false;
			} 
			else if ((character & 0xE0) == 0xE0) 
			{
				characters_remaining_ = 2;
				unicode_ = (character & 0x0F);
			} 
			else if (((character & 0xC0) == 0xC0) && ((character & 0x3E) != 0)) 
			{
				characters_remaining_ = 1;
				unicode_ = (character & 0x1F);
			} 
			else 
			{
				unicode_ = 0;
				res = false;
			}
		} 
		else 
		{
			if ((character & 0xC0) == 0x80 && ( unicode_ != 0 || characters_remaining_ == 1 || ((character & 0x20) != 0) )) 
			{
				unicode_ = (unicode_ << 6) | (character & 0x3F);
				--characters_remaining_;
			} 
			else 
			{
				characters_remaining_ = 0;
				unicode_ = 0;
				res = false;
			}
		}
		return res;
	}
	// returns true if the unicode character has been completely constructed
	bool complete() const { return characters_remaining_ == 0; }
	// returns the unicode character - complete must be true for this value to be meaningful
	Unicode_char unicode() const { return unicode_; }

private:
	Unicode_char unicode_;
	unsigned int characters_remaining_;
};

inline Unicode_string to_unicode( std::string utf8_str )
{
	Unicode_string uni;
	if (!utf8_str.empty())
	{
		std::string::iterator i = utf8_str.begin();
		std::string::iterator e = utf8_str.end();
		UTF8_converter conv;
		for (; i != e; ++i) 
		{
			if (conv.add( *i ) && conv.complete()) 
			{
				Unicode_char c = conv.unicode();
				uni.append( &c, 0, 1 );
			}
		}
	}
	return uni;
}

std::string to_utf8( const Unicode_string& uni );

Unicode_string single_byte_to_unicode( const std::string& s );

std::string unicode_to_single_byte( const Unicode_string& s );

/// \return false if the given string cannot be represented as a single-byte encoded string
bool unicode_to_single_byte( const Unicode_string& s, std::string& result );

enum Byte_order { BIG_ENDIAN , LITTLE_ENDIAN };

/// \brief Convert a unicode string to an array of unsigned bytes, with two bytes
/// for every Unicode_char
///
/// \param order The byte order of the return value
std::vector<uint8> unicode_to_double_byte( const Unicode_string& s, Byte_order order );

/// \param order The byte order of s
///
/// \throw Exception if s does not contain an even number of characters
Unicode_string double_byte_to_unicode( const std::vector<uint8>& s, Byte_order order );

}; // end namespace public_nbs

//---------------------------------------------------------------------------
#endif
//---------------------------------------------------------------------------
#ifndef directoryH
#define directoryH
//---------------------------------------------------------------------------
#include "unicode.h"
#include <algorithm>
#include <vector>
#include <ostream>
#include <functional>
#include "string_utilities.h"
//---------------------------------------------------------------------------

namespace public_nbs {

class Filename;

/// \brief Contains a Unicode (long) directory name string
///
/// The directory name may be local or complete.
/// This class assumes all names given to it directly are long filenames. It
/// subsequently only returns long names. Slashes will be converted to \ however.
/// The case of the name is preserved, but ignored for comparison purposes.
class Directory
{
public:
	Directory() : dir_( L"." ) {}

	/// Converts slashes to '\'
	/// s must be long directory name!
	Directory( const wchar_t* s ) { set( s ); }

	/// Converts slashes to '\'
	/// s must be long directory name!
	Directory( const Unicode_string& s ) { set( s.c_str() ); }

	/// Converts to unicode and slashes to '\'
	/// s must be long directory name!
	Directory( const char* s ) { set( to_unicode(s).c_str() ); }

	/// Converts to unicode and slashes to '\'
	/// s must be long directory name!
	Directory( const std::string& s ) { set( to_unicode(s).c_str() ); }

	/// Converts slashes to '\'
	/// Unicode_string( first, last ) must be long directory name!
	Directory( Unicode_string::const_iterator first, Unicode_string::const_iterator last ) { set( Unicode_string( first, last ).c_str() ); }

	/// replaces any . or .. contained in the directory name
	void clean();

	/// Converts slashes to '\'
	/// d must be long directory name!
	Directory& operator=( const wchar_t* d ) { set( d ); return *this; }

	/// Converts slashes to '\'
	/// s must be long directory name!
	Directory& operator=( const Unicode_string& s ) { set( s.c_str() ); return *this; }

	/// \brief True if a directory with that name exists
	bool exists() const;

	/// \brief Returns filenames of files in the directory, if it exists
	void get_files( std::vector<Filename>& filenames ) const;

	//relative returns the relative path to dir from argument d.
	//Directory("C:\1\2\3").relative("C:\1\b\c"); //returns "..\..\2\3"
	//Directory("C:\1\2\3").relative("D:\1\b\c"); //returns "C:\1\2\3"
	Directory relative( const Directory& d ) const;

	// If d is a relative directory returns the directory d relative
	// to the contained directory
	Directory append( const Directory& d ) const;

	/// \brief Filename for a local filename in the directory
	Filename get_path( const Filename& filename ) const;

	/// \brief Makes a Directory object, ensuring that the "name" string is
	/// converted to a long directory name if necessary
	///
	/// \param force_it Whether to force it to make the call, even if is_short_name()
	///
	/// \warning If "name" indicates a location on the network, it could take
	/// considerable time for this to return
	static Directory make_long_directory( const Unicode_string& name, bool force_it = false );

	/// returns drive as "C:\" or "\\ShareName"
	/// returns "." if directory is not complete
	Directory drive() const;

	/// return true if dir is a fully qualified directory name
	bool is_complete() const;

	/// return a const Unicode_char*
	/// always uses '\' instead of '/'
	/// Only drive roots ("C:\") end with a slash
	const wchar_t* uni_str() const { return dir_.c_str(); }

	/// always uses '/' instead of '\'
	/// Drive roots are discarded except for a leading '/'
	Unicode_string unix_uni_str() const;

	/// return dir_ as a string ensuring there is a trailing backslash "\"
	Unicode_string with_slash() const 
	{
		Unicode_string result( dir_ );
		if (result[result.size() - 1] != L'\\') 
		{
			result += L'\\';
		}
		return result;
	}

	/// always uses '/' instead of '\'
	/// Drive roots are discarded except for a leading '/'
	/// There is a trailing slash '/'
	const Unicode_string unix_with_slash() const
	{
		Unicode_string result = unix_uni_str();
		if (result[result.size() - 1] != L'/') {
			result += L'/';
		}
		return result;
	}

	/// traverse up dir_ "levels" levels and return the result (ex1: Directory("c:\a\b\c").path_up(2) = "c:\a")
	Directory path_up( unsigned int levels = 1 ) const;

	/// \brief Creates a directory with this name
	///
	/// \auto_create_parents If true, non-existent parent directories of the
	/// named directory will also be created
	///
	/// \throw Exception if the directory name is not complete, a directory by that name already
	/// exists, or some other error occurs
	void create( bool auto_create_parents = true ) const;

	/// \brief Removes the directory. Nothing happens if the directory does not exist or is a root drive
	///
	/// \throw Exception on failure
	void remove() const;

	/// return how many folders deep dir i
	/// drive or share root directory returns 0
	unsigned int depth() const;

	/// \brief Value used in all comparisons
	const Unicode_string& comparison_dir() const 
	{
		if (comparison_dir_cache_.empty()) 
		{
			get_long_path_name( dir_, comparison_dir_cache_ );
			to_lower(comparison_dir_cache_);
		}
		return comparison_dir_cache_;
	}

private:
	void set( const wchar_t* d );

	/// must not be null
	/// whenever dir_ is modified, must call invalidate_cache() to clear compare_dir_cache_
	Unicode_string dir_;

	/// cached comparison version of dir_ (to_lower(make_long_directory(dir_)))
	/// empty() means invalidated and needs to be recreated
	mutable Unicode_string comparison_dir_cache_;

	/// \brief Calls GetLongPathNameNBS() on name and puts result in rval
	static void get_long_path_name( const Unicode_string& name, Unicode_string& rval );

	/// \brief Invalidate any caches (currently only comparison_dir_cache_)
	void invalidate_cache() { comparison_dir_cache_.clear(); }
};

inline bool operator==( const Directory& d1, const Directory& d2 )
{
	return d1.comparison_dir() == d2.comparison_dir();
}

inline bool operator!=( const Directory& d1, const Directory& d2 )
{
	return !(d1 == d2);
}

/// \note The < is required for use in std:set
inline bool operator<( const Directory& d1, const Directory& d2 )
{
	return d1.comparison_dir() < d2.comparison_dir();
}

//---------------------------------------------------------------------------

class Filename
{
public:
	Filename() {}
	Filename( const wchar_t* f ) { set( f ); }
	Filename( const Unicode_string& s ) { set( s.c_str() ); }
	Filename( const char* s ) { set( to_unicode(s).c_str() ); }
	Filename( const std::string& s ) { set( to_unicode(s).c_str() ); }

	const Filename operator+( const wchar_t* rhs ) const { return Filename( file + rhs ); }
	const Filename operator+( const Unicode_string& rhs ) const { return Filename( file + rhs ); }
	void operator+=( const wchar_t* rhs ) { file += rhs; }

	bool exists() const;
	bool read_only() const;
	const wchar_t* c_str() const { return file.c_str(); }
	const Unicode_string& str() const { return file; }

	/// always uses '/' instead of '\'
	/// Drive roots are discarded except for a leading '/'
	Unicode_string unix_str() const { return Directory( file ).unix_uni_str(); }

	bool empty() const { return file.empty(); }
	bool is_complete() const;
	Filename relative( const Directory& dir ) const;
	Unicode_string local() const;
	Unicode_string local_no_ext() const;
	
	/// \return does not include "."
	Unicode_string extension() const;

	/// \return Filesize in bytes if the file exists,
	/// -1 if the file information could not be accessed
	sint64 filesize() const;

	static Filename make_long_filename( const Unicode_string& name, bool force_it = false );

	void add_ext( const wchar_t* f )  { file += L'.'; file += f; }
	Directory dir() const;
	Directory drive() const { return Directory(file).drive(); }

	/// \brief Strip off any extension
	///
	/// \param strip_period Whether to strip off the period of the extension
	Filename& strip_ext( bool strip_period = true );

	/// \brief Delete the file
	///
	/// \throw Exception on failure
	void remove() const;
	
private:
	void set( const wchar_t* f );
	Unicode_string file;
	friend bool operator<( const Filename& f1, const Filename& f2 );
};

/// \throw Exception if copying the file failed
void copy_file( const Filename& source, const Filename& dest );

inline bool operator<( const Filename& f1, const Filename& f2 )
{
	return f1.file < f2.file;
}

struct Filename_ci_less : public std::binary_function<Filename, Filename, bool> 
{
    bool operator()(
        const Filename& left, 
        const Filename& right
    ) const
	{
		return get_lower( left.str() ) < get_lower( right.str() );
	}
};


bool operator==( const Filename& f1, const Filename& f2 );

inline bool operator!=( const Filename& f1, const Filename& f2 )
{ return !(f1 == f2); }

inline std::ostream& operator<<(std::ostream& stream, Directory& directory)
{
	stream << to_utf8(directory.uni_str());
	return stream;
}

inline std::wostream& operator<<(std::wostream& stream, Directory& directory)
{
	stream << directory.uni_str();
	return stream;
}

inline std::ostream& operator<<(std::ostream& stream, Filename& filename)
{
	stream << to_utf8(filename.c_str());
	return stream;
}        

inline std::wostream& operator<<(std::wostream& stream, Filename& filename)
{
	stream << filename.c_str();
	return stream;
}        

inline unsigned int find_first_diff( const Unicode_string& s1, const Unicode_string& s2 )
{
	unsigned int pos = 0, pos_at_slash = 0;
	while(s1[pos] == s2[pos] && pos < s1.size() && pos < s2.size())
	{
		if (s1[pos] == '\\') pos_at_slash = pos;
		pos++;
	}
	return pos_at_slash + 1;
}

// Iterator points to object with const Unicode_char* c_str() method
template <class Iterator>
	Directory path_root(Iterator begin, Iterator end)
{
	Unicode_string out;
	if (begin != end)
	{
		out = Filename(begin->c_str()).dir().with_slash().c_str();
		for(Iterator i = begin; i != end; i++)
		{
			out = out.substr(0, find_first_diff(out, i->c_str()));
		}
	}
	return out;
}

Filename append_counter( const Filename& file );

/// \brief Indicates if a directory name or filename is short (e.g. "C:\blahsh~1\blah")
bool is_short_name( const Unicode_string& name );

}; // end namespace public_nbs

//---------------------------------------------------------------------------
#endif

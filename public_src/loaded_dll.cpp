//---------------------------------------------------------------------------
#include "loaded_dll.h"
#include "operating_system.h"
using namespace public_nbs;
//---------------------------------------------------------------------------  
//
// class Loaded_dll
//
//---------------------------------------------------------------------------

Loaded_dll::Loaded_dll( const std::wstring& filename ) : dll_( NULL )
{
	dll_ = LoadLibraryW( filename.c_str() );
	if (dll_ == NULL)
	{
		std::wstring error = L"The file " + filename + 
			L" could not be loaded.\nWindows returned the following error:\n";
		error += Operating_system::singlet().get_last_error();
		throw public_nbs::Exception( error.c_str() );
	}
}
   
//---------------------------------------------------------------------------

Loaded_dll::~Loaded_dll()
{
	FreeLibrary( dll_ );
}

//---------------------------------------------------------------------------

void* Loaded_dll::get_proc_address( const char* name )
{
	return GetProcAddress( dll_, name );
}
                   
//---------------------------------------------------------------------------

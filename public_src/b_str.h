//---------------------------------------------------------------------------
#ifndef b_strH
#define b_strH
//---------------------------------------------------------------------------
#include "windows.h"
#include "ole2.h"
//---------------------------------------------------------------------------

namespace public_nbs {

/// Wrapper around BSTR's allocated with SysAllocString. Mainly, it handles
/// freeing the string in the destructor
class B_str
{
public:
	B_str( const wchar_t* str ) : str_( SysAllocString( str ) ) {}

	B_str( const std::wstring& str ) : str_( SysAllocString( str.c_str() ) ) {}

	B_str() : str_( 0 ) {}

	BSTR* assignee()
	{
		clear();
		return &str_;
	}

	~B_str()
	{ 
		clear(); 
	}

	std::wstring str() const
	{ 
		return str_;
	}

	BSTR b_str() const 
	{ 
		return str_; 
	}

private:
	B_str( const B_str& );
	void operator=( const B_str& );
	void clear()
	{
		if (str_)
		{
			SysFreeString( str_ );
		}
		str_ = 0;
	}

	BSTR str_;
};

} // end namespace public_nbs

//---------------------------------------------------------------------------
#endif

//---------------------------------------------------------------------------
#include "operating_system.h"
#include "windows.h"
#include "nbs/ptr.h"
using namespace public_nbs;
//---------------------------------------------------------------------------
//
// Operating_system
//
//---------------------------------------------------------------------------

Operating_system& Operating_system::singlet()
{
   static Operating_system instance;
   return instance;
}

//---------------------------------------------------------------------------

Operating_system::Operating_system() : win9x( false ), winnt( false ), 
	winxp( false ), vista( false ), win7( false ), bit_64( false )
{
	OSVERSIONINFO info;
	info.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	GetVersionEx(&info);

	switch (info.dwPlatformId) {
	case VER_PLATFORM_WIN32_WINDOWS:
		win9x = true;
		if (info.dwMinorVersion == 0) ver = W95;
		else ver = W98_ME;
		break;
	case VER_PLATFORM_WIN32_NT:
		winnt = true;
		if( info.dwMajorVersion < 5 ) {
			ver = WNT;
		} else {
			ver = W2000;
			if( info.dwMajorVersion == 5 && info.dwMinorVersion >= 1 ) {
				winxp = true;
			}
			if( info.dwMajorVersion == 6 && info.dwMinorVersion == 0  ) {
				vista = true;
			}
			if( (info.dwMajorVersion == 6 && info.dwMinorVersion >= 1) || info.dwMajorVersion > 6 ) {
				win7 = true;
			}
		}
		break;
	default: ver = OTHER;
	}

	HMODULE kernel = GetModuleHandleW( L"Kernel32.dll" );
	if (kernel)
	{
		typedef BOOL (WINAPI* Fun)( HANDLE hProcess, PBOOL Wow64Process );
		Fun fun = (Fun)GetProcAddress( kernel, "IsWow64Process" );
		if (fun)
		{
			BOOL is_wow64;
			if (fun( GetCurrentProcess(), &is_wow64 ))
			{
				bit_64 = (is_wow64 != FALSE);
			}
		}
	}
}

//---------------------------------------------------------------------------
   
std::wstring Operating_system::process_current_directory() const
{
	std::wstring rval;

	unsigned int buffer_size = GetCurrentDirectoryW( 0, 0 );
	if (buffer_size > 0)
	{
		std::unique_ptr<wchar_t[]> buffer( new wchar_t[buffer_size] );
		if (GetCurrentDirectoryW( buffer_size, buffer.get() ) > 0)
		{
			buffer[buffer_size-1] = 0; // just in case
			rval = buffer.get();
		}
	}

	return rval;
}

//---------------------------------------------------------------------------

std::wstring Operating_system::get_last_error() const
{
	return get_error_message( GetLastError() );
}

//---------------------------------------------------------------------------

std::wstring Operating_system::get_error_message( HRESULT code ) const
{
	LPVOID lpMsgBuf;
	std::wstring rval;

	DWORD char_count = FormatMessageW( FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM |
						FORMAT_MESSAGE_IGNORE_INSERTS, NULL, code,
						MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
						(LPWSTR) &lpMsgBuf, 0, NULL );
	if( char_count && lpMsgBuf ) {
		rval = (LPWSTR)lpMsgBuf;
		LocalFree( lpMsgBuf );// Free the buffer.
	} else {
		rval = L"FormatMessage() was unable to process the error code";
	}
	return rval;
}

//---------------------------------------------------------------------------



























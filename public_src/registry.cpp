//---------------------------------------------------------------------------
#include "registry.h"
#include "string_utilities.h"
#include "exception.h"
//---------------------------------------------------------------------------
using namespace public_nbs;
//---------------------------------------------------------------------------

namespace {

	std::wstring format_error( LONG code )
	{
		std::wstring rval;

		wchar_t* buffer; 
		if ( FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, 0,
			code, 0, (LPTSTR)&buffer, 0, 0 ) )
		{
			rval = buffer;
			LocalFree( buffer );
		}	
		return rval;
	}

}

//---------------------------------------------------------------------------
//
// class Registry_key
//
//---------------------------------------------------------------------------

void Registry_key::get_subkeys( std::vector<std::wstring>& keys )
{
	wchar_t buffer[256];
	DWORD size = 256;
	FILETIME time;
	DWORD index = 0;
	LONG res = RegEnumKeyExW( handle_, index, buffer, &size, NULL, NULL, NULL, &time );
	while (res == ERROR_SUCCESS)
	{
		keys.push_back( buffer );
		++index;
		size = 256;
		res = RegEnumKeyExW( handle_, index, buffer, &size, NULL, NULL, NULL, &time );
	}
	if (res != ERROR_NO_MORE_ITEMS)
	{
		throw Exception( format_error( res ) );
	}
}

//---------------------------------------------------------------------------

void Registry_key::get_values( std::vector<std::wstring>& values )
{
	const unsigned int BUFFER_SIZE = 256;
	wchar_t buffer[BUFFER_SIZE + 1];
	buffer[BUFFER_SIZE] = 0;
	DWORD size = BUFFER_SIZE;
	DWORD index = 0;
	LONG res = RegEnumValueW( handle_, index, buffer, &size, NULL, NULL, NULL, NULL );
	while (res == ERROR_SUCCESS)
	{
		values.push_back( buffer );
		++index;
		size = BUFFER_SIZE;
		res = RegEnumValueW( handle_, index, buffer, &size, NULL, NULL, NULL, NULL );
	}
	if (res != ERROR_NO_MORE_ITEMS)
	{
		throw Exception( format_error( res ) );
	}
}

//---------------------------------------------------------------------------

bool Registry_key::subkey_exists( const std::wstring& name )
{
	bool exists = true;
	try
	{
		Ptr<Registry_key>::Shared_ptr key = open_subkey( name, false, false );
	}
	catch (Exception&)
	{
		exists = false;
	}
	return exists;
}

//---------------------------------------------------------------------------

void Registry_key::delete_subkey( const std::wstring& name )
{
	if (subkey_exists( name ))
	{
		Ptr<Registry_key>::Shared_ptr key = open_subkey( name, true, false );
		std::vector<std::wstring> subkeys;
		key->get_subkeys( subkeys );
		for (unsigned int i = 0; i < subkeys.size(); ++i)
		{
			key->delete_subkey( subkeys[i] );
		}
		LONG res = RegDeleteKeyW( handle_, name.c_str() );
		if (res != ERROR_SUCCESS)
		{
			throw Exception( format_error( res ) );
		}
	}
}

//---------------------------------------------------------------------------

void Registry_key::set_value( const std::wstring& name, double value )
{
	LONG res = RegSetValueExW( handle_, name.c_str(), 0, REG_BINARY, 
		(const BYTE*)&value, sizeof( double ) );
	if (res != ERROR_SUCCESS)
	{
		std::wstring error = L"RegSetValueEx failed - " + name + L"," + 
			to_wstr( value ) + L" - " + format_error( res );
		throw Exception( error );
	}
}

//---------------------------------------------------------------------------

void Registry_key::set_value( const std::wstring& name, 
	const std::wstring& value )
{
	LONG res = RegSetValueExW( handle_, name.c_str(), 0, REG_SZ, 
		(const BYTE*)value.c_str(), 
		DWORD((value.size() + 1) * sizeof(wchar_t)) );
	if (res != ERROR_SUCCESS)
	{
		std::wstring error = L"RegSetValueEx failed - " + name + L"," + 
			value + L" - " + format_error( res );
		throw Exception( error );
	}
}

//---------------------------------------------------------------------------

void Registry_key::set_value( const std::wstring& name, 
	uint32 value )
{
	LONG res = RegSetValueExW( handle_, name.c_str(), 0, REG_DWORD,
		(const BYTE*)&value, sizeof(value) );
	if (res != ERROR_SUCCESS)
	{
		std::wstring error = L"RegSetValueEx failed - " + name + L"," + 
			to_wstr(value) + L" - " + format_error( res );
		throw Exception( error );
	}
}

//---------------------------------------------------------------------------

void Registry_key::delete_value( const std::wstring& name )
{
	LONG res = RegDeleteValueW( handle_, name.c_str() );
	if (res != ERROR_SUCCESS)
	{
		std::wstring error = L"RegDeleteValue failed - " + name + L" - " + 
			format_error( res );
		throw Exception( error );
	}
}

//---------------------------------------------------------------------------
	
std::wstring Registry_key::get_string( const std::wstring& name )
{
	std::wstring rval;

	const unsigned int BUFFER_SIZE = 1024;
	wchar_t buffer[BUFFER_SIZE];

	DWORD type;
	DWORD size = sizeof(wchar_t) * BUFFER_SIZE;
	LONG res = RegQueryValueExW( handle_, name.c_str(), NULL, &type,
		(LPBYTE)&buffer, &size );
	if ((res == ERROR_SUCCESS) && (type == REG_SZ))
	{
		rval = buffer;
	}
	else if (res == ERROR_MORE_DATA && type == REG_SZ)
	{
		std::unique_ptr<wchar_t[]> buffer2( new wchar_t[size / 2 + 1]);
		res = RegQueryValueExW( handle_, name.c_str(), NULL, &type,
			(LPBYTE)buffer2.get(), &size );
		if (res == ERROR_SUCCESS)
		{
			rval = buffer2.get();
		}
	}

	return rval;
}

//---------------------------------------------------------------------------
	
double Registry_key::get_double( const std::wstring& name )
{
	double value = 0;
	DWORD type;
	DWORD size = sizeof( value );
	LONG res = RegQueryValueExW( handle_, name.c_str(), NULL, &type,
		(LPBYTE)&value, &size );

	return value;
}

//---------------------------------------------------------------------------

uint32 Registry_key::get_int32( const std::wstring& name )
{
	uint32 value = 0;
	DWORD type;
	DWORD size = sizeof( value );
	LONG res = RegQueryValueExW( handle_, name.c_str(), NULL, &type,
		(LPBYTE)&value, &size );

	return value;
}

//---------------------------------------------------------------------------

Registry_key::Value_type Registry_key::get_type( const std::wstring& name )
{
	Value_type rval = NO_VALUE;

	DWORD type;
	if (RegQueryValueExW( handle_, name.c_str(), NULL, &type, 0, 0 ) == ERROR_SUCCESS)
	{
		switch (type)
		{
		case REG_SZ:
			rval = STRING; 
			break;
		case REG_DWORD:
			rval = UINT32; 
			break;
		case REG_BINARY: 
			rval = BINARY; 
			break;
		case REG_EXPAND_SZ:
		case REG_LINK:
		case REG_MULTI_SZ:
			rval = OTHER; 
			break;	
		case REG_NONE:
			rval = NONE; 
			break;
		case REG_QWORD:
			rval = UINT64; 
			break;
		}
	}

	return rval;
}

//---------------------------------------------------------------------------

Ptr<Registry_key>::Shared_ptr Registry_key::open_subkey( 
	const std::wstring& name, bool write, bool create )
{
	Ptr<Registry_key>::Shared_ptr key;

	REGSAM security = KEY_READ;
	if (write) security = KEY_ALL_ACCESS;

	HKEY new_key;
	LONG res;
	if (create)
	{
		res = RegCreateKeyExW( handle_, name.c_str(), 0, NULL,
			REG_OPTION_NON_VOLATILE, security, NULL, &new_key, NULL );
	}
	else
	{
		res = RegOpenKeyExW( handle_, name.c_str(), 0, security, &new_key );
	}
	if (res == ERROR_SUCCESS)
	{
		key.reset( new Registry_key( new_key ) );
	}
	else
	{
		std::wstring error = L"RegOpenKeyEx failed - " + name + L" - " + format_error( res );
		throw Exception( error );
	}

	return key;
}

//---------------------------------------------------------------------------
//
// class Registry
//
//---------------------------------------------------------------------------

Registry& Registry::singlet()
{
	static Registry instance;
	return instance;
}

//---------------------------------------------------------------------------

HKEY Registry::root_key( Root_type type )
{
	HKEY root_key = HKEY_CLASSES_ROOT;
	switch (type) 
	{
		case CLASSES: 
			root_key = HKEY_CLASSES_ROOT; break; 
		case CURRENT_USER: 
			root_key = HKEY_CURRENT_USER; break;
		case LOCAL_MACHINE: 
			root_key = HKEY_LOCAL_MACHINE; break;
		case USERS:
			root_key = HKEY_USERS; break;
	}
	return root_key;
}

//---------------------------------------------------------------------------

Ptr<Registry_key>::Shared_ptr Registry::open_key( Root_type root, 
		const std::wstring& name, bool write,  bool create )
{
	Registry_key temp( root_key( root ) );
	return temp.open_subkey( name, write, create );
}

//---------------------------------------------------------------------------

bool Registry::key_exists( Root_type root, const std::wstring& name )
{
	Registry_key temp( root_key( root ) );
	return temp.subkey_exists( name );
}

//---------------------------------------------------------------------------

void Registry::delete_key( Root_type root, const std::wstring& name )
{
	Registry_key temp( root_key( root ) );
	temp.delete_subkey(  name );
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
#include "..\nbs\precomp.h"
#pragma hdrstop
//---------------------------------------------------------------------------
#include "dotnet_utilities.h"
#include "com_initializer.h"
#include "string_utilities.h"
//---------------------------------------------------------------------------
using namespace public_nbs;
using namespace System::Text;
//---------------------------------------------------------------------------

std::string public_nbs::String_to_utf8( String^ s )
{
	UTF8Encoding^ utf8 = gcnew UTF8Encoding();
	array<Byte>^ a = utf8->GetBytes( s );
	std::string ret;
	for (int i = 0; i < a->Length; ++i)
	{
		ret.push_back( a[i] );
	}
	return ret;
}

//---------------------------------------------------------------------------

String^ public_nbs::utf8_to_String( const std::string& s )
{
	array<Byte>^ a = gcnew array<Byte>( s.length() );
	for (unsigned int i = 0; i < s.length(); ++i)
	{
		a[i] = s[i];
	}
	UTF8Encoding ^ utf8 = gcnew UTF8Encoding();
	return utf8->GetString( a );
}

//---------------------------------------------------------------------------

std::wstring public_nbs::String_to_unicode( String^ s )
{
	std::wstring ret( s->Length, 0 );
	array<wchar_t, 1>^ a = s->ToCharArray();
	for (int i = 0; i < a->Length; ++i)
	{
		ret[i] = a[i];
	}
	return ret;
}

//---------------------------------------------------------------------------

String^ public_nbs::unicode_to_String( const std::wstring& s )
{
	return gcnew String( s.c_str() );
}

//---------------------------------------------------------------------------

sint8 public_nbs::String_to_sint8( String^ s, sint8 def )
{
	try
	{
		def = Convert::ToSByte( s );
	}
	catch (...) {}
	return def;
}

//---------------------------------------------------------------------------

uint8 public_nbs::String_to_uint8( String^ s, uint8 def )
{
	try
	{
		def = Convert::ToByte( s );
	}
	catch (...) {}
	return def;
}

//---------------------------------------------------------------------------

sint16 public_nbs::String_to_sint16( String^ s, sint16 def )
{
	try
	{
		def = Convert::ToInt16( s );
	}
	catch (...) {}
	return def;
}

//---------------------------------------------------------------------------

uint16 public_nbs::String_to_uint16( String^ s, uint16 def )
{
	try
	{
		def = Convert::ToUInt16( s );
	}
	catch (...) {}
	return def;
}

//---------------------------------------------------------------------------

sint32 public_nbs::String_to_sint32( String^ s, sint32 def )
{
	try
	{
		def = Convert::ToInt32( s );
	}
	catch (...) {}
	return def;
}

//---------------------------------------------------------------------------

uint32 public_nbs::String_to_uint32( String^ s, uint32 def )
{
	try
	{
		def = Convert::ToUInt32( s );
	}
	catch (...) {}
	return def;
}

//---------------------------------------------------------------------------

sint64 public_nbs::String_to_sint64( String^ s, sint64 def )
{
	try
	{
		def = Convert::ToInt64( s );
	}
	catch (...) {}
	return def;
}

//---------------------------------------------------------------------------

uint64 public_nbs::String_to_uint64( String^ s, uint64 def )
{
	try
	{
		def = Convert::ToUInt64( s );
	}
	catch (...) {}
	return def;
}

//---------------------------------------------------------------------------

double public_nbs::String_to_double( String^ s, double def )
{
	try
	{
		def = Convert::ToDouble( s );
	}
	catch (...) {}
	return def;
}

//---------------------------------------------------------------------------
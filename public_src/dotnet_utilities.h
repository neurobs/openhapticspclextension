//---------------------------------------------------------------------------
#ifndef dotnet_utilitiesH
#define dotnet_utilitiesH
//---------------------------------------------------------------------------
#include <string>
#include "types.h"
#include "nbs/ptr.h"
#include "com_ptr.h"
#include "exception.h"
//---------------------------------------------------------------------------
using namespace System;
#pragma warning(disable:4383)
//---------------------------------------------------------------------------

namespace public_nbs {

/// \brief Convert between managed and unmanaged string types
std::string String_to_utf8( String^ s );
String^ utf8_to_String( const std::string& s );
std::wstring String_to_unicode( String^ s );
String^ unicode_to_String( const std::wstring& s );

/// \brief Convert managed string to a number
///
/// \param def value to return if the string-to-number conversion fails
sint8 String_to_sint8( String^ s, sint8 def );
uint8 String_to_uint8( String^ s, uint8 def );
sint16 String_to_sint16( String^ s, sint16 def );
uint16 String_to_uint16( String^ s, uint16 def );
sint32 String_to_sint32( String^ s, sint32 def );
uint32 String_to_uint32( String^ s, uint32 def );
sint64 String_to_sint64( String^ s, sint64 def );
uint64 String_to_uint64( String^ s, uint64 def );
double String_to_double( String^ s, double def );

} // end namespace public_nbs

//---------------------------------------------------------------------------
#endif
//---------------------------------------------------------------------------
#ifndef loaded_dllH
#define loaded_dllH
//--------------------------------------------------------------------------- 
#include <windows.h>
#include <string>
#include "exception.h"
//---------------------------------------------------------------------------

namespace public_nbs {

class Loaded_dll
{
public:
   // throws public_nbs::Exception
	Loaded_dll( const std::wstring& filename );
   ~Loaded_dll();

   // returns zero on error
   void* get_proc_address( const char* name );

private:
   HMODULE dll_;
};

} // namespace nbs

//---------------------------------------------------------------------------
#endif

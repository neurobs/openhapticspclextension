//--------------------------------------------------------------------------- 
#include "unicode.h"
#include "exception.h"
#include "windows.h"
#include "nbs/ptr.h"
#include <limits>
//--------------------------------------------------------------------------- 
using namespace public_nbs;
using namespace std;
//---------------------------------------------------------------------------

std::string public_nbs::to_utf8( const Unicode_string& uni )
{
	std::string ret;
	int count = int( uni.size() * 3 + 3 );
	std::unique_ptr<char[]> buffer( new char[count] );
	count = WideCharToMultiByte( CP_UTF8, 0, uni.c_str(), -1, buffer.get(), count, NULL, NULL );
	if (count > 0) 
	{
		buffer[count - 1] = 0;
		ret = buffer.get();
	}
	return ret;
}

//---------------------------------------------------------------------------

bool public_nbs::unicode_to_single_byte
( 
	const Unicode_string& s, 
	std::string& result 
)
{
	bool success = true;
	result.resize( s.size() );
	for (unsigned int i = 0; success && i < s.size(); ++i)
	{
		result[i] = static_cast<std::string::value_type>( s[i] );
		success = ( (s[i] >= (numeric_limits<std::string::value_type>::min)()) &&
			(s[i] <= (numeric_limits<std::string::value_type>::max)()) );
	}
	return success;
}

//---------------------------------------------------------------------------

namespace {

Unicode_char switch_bytes( Unicode_char c )
{
	Unicode_char ret = c >> 8;
	ret |= c << 8;
	return ret;
}

}; // unnamed namespace

//---------------------------------------------------------------------------

Unicode_string public_nbs::double_byte_to_unicode( const std::vector<uint8>& s, Byte_order order )
{
	if (s.size() % 2 != 0)
	{
		throw Exception( L"Cannot convert string to Unicode" );
	}
	Unicode_string ret;
	std::vector<uint8>::const_iterator i = s.begin();
	for (; i != s.end(); ++i)
	{
		Unicode_char temp = static_cast<Unicode_char>(*i);
		temp <<= 8;
		++i;
		temp |= static_cast<Unicode_char>(*i);
		if (order == LITTLE_ENDIAN)
		{
			temp = switch_bytes( temp );
		}
		ret.push_back( temp );
	}
	return ret;
}

//---------------------------------------------------------------------------

std::vector<uint8> public_nbs::unicode_to_double_byte( const Unicode_string& s, Byte_order order )
{
	std::vector<uint8> ret;
	Unicode_string::const_iterator i = s.begin();
	for (; i != s.end(); ++i)
	{
		Unicode_char temp = *i;
		if (order == BIG_ENDIAN)
		{
			temp = switch_bytes( temp );
		}
		ret.push_back( static_cast<uint8>( temp ) );
		ret.push_back( static_cast<uint8>( temp  >> 8 ) );
	}
	return ret;
}

//---------------------------------------------------------------------------

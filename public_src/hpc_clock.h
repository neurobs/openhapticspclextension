//---------------------------------------------------------------------------
#ifndef hpc_clockH
#define hpc_clockH
//---------------------------------------------------------------------------
#include "time_micro.h"
#include "types.h"
#include "windows.h"
#include "exception.h"
#include <string>
//---------------------------------------------------------------------------

namespace public_nbs {

/// Clock that uses the CPU high performance counter
class HPC_clock
{
public:
	/// \throw Exception Error from Win API
	HPC_clock();

	/// The current time
	Time_micro time() const;

	/// Resets the clock origin so that now is time t
	void set( Time_micro t );

	/// The number of performance counter ticks in one second
	sint64 counts_per_second() const { return counts_per_second_; }

	struct Overflow : public Exception
	{
		Overflow( LONGLONG min, LONGLONG current, LONGLONG start, const wchar_t* method );
		std::wstring report();
		std::wstring report( sint64 min, sint64 current, sint64 start, const std::wstring& method );
		sint64 min_;
		sint64 current_;
		sint64 start_;
		std::wstring method_;
	};

private:              
	sint64 counts_per_second_;
	sint64 zero_count_;
	sint64 min_count_;
	static const sint64 MICROSECONDS_PER_SECOND = 1000000;
};

} // end namespace public_nbs

//---------------------------------------------------------------------------
#endif
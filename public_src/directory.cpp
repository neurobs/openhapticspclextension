//---------------------------------------------------------------------------
#include "directory.h"
#include "string_utilities.h"
#include "exception.h"
#include <windows.h>
#include "operating_system.h"
#include "shlwapi.h"
#include <list>
//---------------------------------------------------------------------------
using namespace public_nbs;
//---------------------------------------------------------------------------
//
// class utility functions
//
//---------------------------------------------------------------------------

namespace {

	Directory path_up( const Unicode_string path, const unsigned int levels )
	{
		Unicode_string::size_type pos = path.size()-1;
		Unicode_string::size_type last_good_pos = pos;
		for (unsigned int i=0; i<levels && pos != Unicode_string::npos; i++)
		{
			pos = path.rfind(L'\\', pos-1);
			if (pos != Unicode_string::npos)
				last_good_pos = pos;
		}
		return path.substr(0, last_good_pos+1);
	}

} // unnamed namespace

//---------------------------------------------------------------------------

/// \brief Indicates if a directory name or filename is short (e.g. "C:\blahsh~1\blah")
bool public_nbs::is_short_name( const Unicode_string& name )
{
	bool is_short = false;
	Unicode_string::const_iterator i = name.begin();
	Unicode_string::const_iterator e = name.end();
	for ( ; i != e; ++i)
	{
		if (*i == L'~')
		{
			Unicode_string::const_iterator temp = i;
			++temp;
			while (temp != e && (*temp < '0' || *temp > '9'))
			{
				++temp;
			}
			if (to_int_def( Unicode_string( i, temp ).c_str(), -1 ) != -1)
			{
				is_short = true;
				break;
			}
		}
	}
	return is_short;
}

//---------------------------------------------------------------------------
//
// class Directory
//
//---------------------------------------------------------------------------

void Directory::clean()
{
	invalidate_cache();
	std::list<Unicode_string> parts;
	split( dir_.begin(), dir_.end(), L'\\', parts );
	std::list<Unicode_string>::iterator i = parts.begin();
	while (i != parts.end()) 
	{
		if (*i == L".") 
		{
			i = parts.erase( i );
		} 
		else if (*i == L"..") 
		{
			if (i != parts.begin()) 
			{
				std::list<Unicode_string>::iterator k = i;
				--k;
				if (*k != L"..") 
				{
					parts.erase( k );
					i = parts.erase( i );
				} 
				else 
				{
					++i;
				}
			} 
			else 
			{
				++i;
			}
		} 
		else 
		{
			++i;
		}
	}
	dir_.clear();
	i = parts.begin();
	while (i != parts.end()) 
	{
		dir_ += *i;
		++i;
		if (i != parts.end()) 
		{
			dir_ += L'\\';
		}
	}
	if (dir_.empty()) 
	{
		dir_ = L".";
	} 
	else if (dir_[dir_.size()-1] == L':') 
	{
		dir_ += L'\\';
	}
}

//---------------------------------------------------------------------------

void Directory::get_files( std::vector<Filename>& filenames ) const
{
	if (exists())
	{
		HANDLE find_handle;
		WIN32_FIND_DATAW find_data;
		if ((find_handle = FindFirstFileW( get_path( L"*" ).c_str(), &find_data )) != NULL) 
		{
			do 
			{
				if (!(find_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) 
				{
					filenames.push_back( get_path( find_data.cFileName ) );
				}
			} 
			while (FindNextFileW( find_handle, &find_data ) != 0);
			FindClose( find_handle );
		}
	}
}

//---------------------------------------------------------------------------

void Directory::set( const wchar_t* d )
{
	invalidate_cache();
	if (!*d)
	{
		dir_ = L".";
	}
	else
	{
		dir_ = d;
		std::replace( dir_.begin(), dir_.end(), L'/', L'\\' );
		if (dir_[dir_.size() - 1] == L'\\' && dir_.size() > 1 && (dir_.size() != 3 || dir_[1] != ':'))
		{
			dir_.resize( dir_.size() - 1 );
		}
		else
		{
			if (dir_.size() == 2 && dir_[1] == ':' && isalpha( dir_[0] ))
			{
				dir_ += L"\\"; //if it is in the form of "x:", then include the slash
			}
		}
	}
}

//---------------------------------------------------------------------------

Directory Directory::relative( const Directory& d ) const
{
	Directory ret;
	if (is_complete())
	{
		wchar_t szRelativePath[_MAX_PATH];
		Unicode_string sRelPath;

		if (PathRelativePathToW( szRelativePath, d.uni_str(), FILE_ATTRIBUTE_DIRECTORY,
			dir_.c_str(), FILE_ATTRIBUTE_DIRECTORY ) == TRUE)
		{
			sRelPath = szRelativePath;
			if (sRelPath.length() > 1)
			{
				// Remove ".\" from the beginning
				if ((sRelPath[0] == L'.') && (sRelPath[1] == L'\\')) 
				{
					sRelPath = sRelPath.substr(2, sRelPath.length() - 2);
				}
				// Remove "\" from the beginning
				else if( sRelPath[0] == L'\\' ) 
				{
					sRelPath = sRelPath.substr(1, sRelPath.length() - 1);
				}
			}
		} 
		else 
		{
			sRelPath = dir_;
		}
		ret = sRelPath;
	}
	return ret;
}

//---------------------------------------------------------------------------

Directory Directory::drive() const
{
	wchar_t drv[_MAX_DRIVE], direc[_MAX_DIR];
	wchar_t name[_MAX_FNAME], ext[_MAX_EXT];
	_wsplitpath( dir_.c_str(), drv, direc, name, ext );
	Unicode_string sdrv(drv);
	if (sdrv.length() == 0)
	{
		Unicode_string tmp_dir(with_slash());
		if (tmp_dir.length() > 4 && tmp_dir[0] == L'\\' && tmp_dir[1] == L'\\')
		{
			int slashes = 0;
			for (unsigned int i = 3; i < tmp_dir.length() && slashes != 2; ++i)
			{
				if (tmp_dir[i] == L'\\')
				{
					++slashes;
					sdrv = tmp_dir.substr( 0, i );
				} 
				else if (i == tmp_dir.length() - 2)
				{
					sdrv = tmp_dir.substr( 0, i );
				}
			}
		}
	}
	return sdrv;
}

//---------------------------------------------------------------------------

bool Directory::is_complete() const
{
	wchar_t drv[_MAX_DRIVE], direc[_MAX_DIR];
	wchar_t name[_MAX_FNAME], ext[_MAX_EXT];
	_wsplitpath( dir_.c_str(), drv, direc, name, ext );

	return (!Unicode_string(drv).empty()) || (Unicode_string(drv).empty() && direc[0] == L'\\' && direc[1] == L'\\');
}

//---------------------------------------------------------------------------

Unicode_string Directory::unix_uni_str() const
{
	Unicode_string unix_dir = dir_;
	Unicode_string dr( drive().uni_str() );
	if (dr != L".") unix_dir.replace( unix_dir.find( dr ), 3, L"/" );
	std::replace( unix_dir.begin(), unix_dir.end(), L'\\', L'/' );
	return unix_dir;
}

//---------------------------------------------------------------------------

Directory Directory::append( const Directory& d ) const
{
	return get_path( d.uni_str() ).c_str();
}

//---------------------------------------------------------------------------

namespace {

Unicode_string make_long_path_name( const Unicode_string& short_name )
{
	Unicode_string rval;
	Unicode_char* c = 0;
	DWORD size = GetLongPathNameW( short_name.c_str(), c, 0 );
	c = new Unicode_char[size];
	DWORD new_size = GetLongPathNameW( short_name.c_str(), c, size );
	if (new_size == size)
	{
		rval = c;
	}
	delete[] c;
	return rval;
}

} // end unnamed namespace
	
Directory Directory::make_long_directory( const Unicode_string& name, bool force_it )
{
	Unicode_string rval = name;
	if (force_it || is_short_name( rval ))
	{
		rval = make_long_path_name( name );
	}
	return rval;
}

//---------------------------------------------------------------------------

void Directory::get_long_path_name( const Unicode_string& name, Unicode_string& rval )
{
	rval = name;
	if (is_short_name( name )) 
	{
		rval = make_long_path_name( name );
	}
}

//---------------------------------------------------------------------------

bool Directory::exists() const
{
	return is_complete() && PathIsDirectoryW( dir_.c_str() ) != 0;
}

//---------------------------------------------------------------------------

void Directory::create( bool auto_create_parents ) const
{
	if (!is_complete())
	{
		throw Exception( Unicode_string( L"Attempt to create non-complete directory name " ) + with_slash() );
	}
	else if (exists())
	{
		throw Exception( Unicode_string( L"Attempt to create directory that already exists: " ) +
			with_slash() );
	}
	else
	{
		if (auto_create_parents && !path_up().exists())
		{
			path_up().create( true );
		}
		if (CreateDirectoryW( dir_.c_str(), NULL ) == 0)
		{
			throw Exception( Unicode_string( L"Error creating directory " ) +
				with_slash() + L":\n" + Operating_system::singlet().get_last_error() );
		}
	}
}

//---------------------------------------------------------------------------
	
Filename Directory::get_path( const Filename& filename ) const
{
	if (dir_ == L".") return filename;
	Unicode_string s = with_slash() + filename.c_str();
	Filename ret = s;
	if (s.rfind( L"..\\" ) != Unicode_string::npos) 
	{
		Directory d = ret.dir();
		d.clean();
		ret = d.with_slash() + ret.local();
	}
	return ret;
}

//---------------------------------------------------------------------------

void Directory::remove() const
{
	if (exists() && dir_ != drive())
	{
		HANDLE find_handle;
		WIN32_FIND_DATA find_data;
		if ((find_handle = FindFirstFileW( get_path( L"*" ).c_str(), &find_data )) != NULL) 
		{
			do 
			{
				Unicode_string nm( find_data.cFileName );
				if (find_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) 
				{
					if (nm != L"." && nm != L"..") 
					{
						Directory( append( ( nm + L"\\" ).c_str() ) ).remove();
					}
				} 
				else 
				{
					DeleteFileW( append( nm.c_str() ).uni_str() );
				}
			} 
			while (FindNextFileW( find_handle, &find_data ) != 0);
			FindClose( find_handle );
		}
		SetCurrentDirectoryW( path_up().uni_str() );
		if (RemoveDirectoryW( dir_.c_str() ) == 0)
		{
			throw Exception( Unicode_string( L"Error removing directory " ) +
				dir_.c_str() + L":\n" + Operating_system::singlet().get_last_error() );
		}
	}
}

//---------------------------------------------------------------------------

Directory Directory::path_up( unsigned int levels ) const
{
	Unicode_string::size_type pos = with_slash().size() - 1;
	Unicode_string::size_type last_good_pos = pos;
	Unicode_string::size_type root_pos = drive().dir_.size();
	for (Unicode_string::size_type i = 0; i < levels && pos != Unicode_string::npos && pos > root_pos; i++)
	{
		pos = dir_.rfind(L'\\', pos-1);
		if (pos != Unicode_string::npos) last_good_pos = pos;
	}
	return dir_.substr( 0, last_good_pos );
}

//---------------------------------------------------------------------------

unsigned int Directory::depth() const
{
	unsigned int count = 0;
	Unicode_string::size_type pos = with_slash().size() - 1;
	Unicode_string::size_type root_pos = drive().dir_.size();
	for (Unicode_string::size_type i=0; pos != Unicode_string::npos && pos > root_pos; i++)
	{
		pos = dir_.rfind( L'\\', pos - 1 );
		if (pos != Unicode_string::npos) count++;
	}
	return count;
}

//---------------------------------------------------------------------------
//
// class Filename
//
//---------------------------------------------------------------------------

void Filename::set( const wchar_t* f )
{
	file = f ? f : L"";
	std::replace( file.begin(), file.end(), L'/', L'\\' );
}

//---------------------------------------------------------------------------

bool Filename::read_only() const
{
	bool rval = false;
	if (exists()) 
	{
		rval = GetFileAttributesW( file.c_str() ) & FILE_ATTRIBUTE_READONLY;
	}
	return rval;
}

//---------------------------------------------------------------------------

bool Filename::exists() const
{
	bool rval = is_complete();
	if (rval)
	{
		rval = PathIsDirectoryW( file.c_str() ) == 0 &&
			PathFileExistsW( file.c_str() ) != 0;
	}
	return rval;
}

//---------------------------------------------------------------------------

bool Filename::is_complete() const 
{
	return dir().is_complete();
}

//---------------------------------------------------------------------------

Filename Filename::relative( const Directory& d ) const
{
	Filename rval;
	if (is_complete())
	{
		wchar_t buffer[_MAX_PATH];
		Unicode_string rel_path;
		if (PathRelativePathToW( buffer, d.with_slash().c_str(), FILE_ATTRIBUTE_DIRECTORY, file.c_str(), 0 ) == TRUE)
		{
			rel_path = buffer;
			if (rel_path.length() > 1)
			{
				// Remove ".\" from the beginning
				if ((rel_path[0] == L'.') && (rel_path[1] == L'\\'))
				{
					rel_path = rel_path.substr(2, rel_path.length() - 2);
				}
				// Remove "\" from the beginning
				if (rel_path[0] == L'\\') 
				{
					rel_path = rel_path.substr(1, rel_path.length() - 1);
				}
			}
		} 
		else 
		{
			rel_path = file;
		}
		rval = rel_path;
	}
	return rval;
}

//---------------------------------------------------------------------------

Unicode_string Filename::extension() const
{
	Unicode_string rval;
	Unicode_string::size_type posd = file.rfind(L'.'); //position of last dot '.'
	if (posd != Unicode_string::npos)
	{          
		Unicode_string::size_type poss = file.rfind( L'\\' );
		if (poss == Unicode_string::npos || posd > poss)
		{
			rval = file.substr( posd + 1 );
		}
	}
	return rval;
}

//---------------------------------------------------------------------------

sint64 Filename::filesize() const
{
	sint64 rval = -1;
	WIN32_FILE_ATTRIBUTE_DATA file_info;
	if (exists() && GetFileAttributesExW( file.c_str(), GetFileExInfoStandard, static_cast<void *>( &file_info ) ) &&
		file_info.nFileSizeHigh == 0)
	{
		rval = static_cast<sint64>( file_info.nFileSizeLow );
	}
	return rval;
}

//---------------------------------------------------------------------------
	
Filename Filename::make_long_filename( const Unicode_string& name, bool force_it )
{
	Unicode_string rval = name;
	if (force_it || is_short_name( name ))
	{
		rval = make_long_path_name( name );
	}
	return rval;
}

//---------------------------------------------------------------------------

Unicode_string Filename::local() const
{
	Unicode_string rval( file );
	Unicode_string::size_type pos = rval.rfind( L'\\' );//position of last slash '\'
	if (pos != Unicode_string::npos) rval = rval.substr( pos + 1 );
	return rval;
}

//---------------------------------------------------------------------------

Unicode_string Filename::local_no_ext() const
{
	Filename rval( local() );
	return rval.strip_ext().c_str();
}

//---------------------------------------------------------------------------

Directory Filename::dir() const
{
	Directory rval;
	Unicode_string::size_type pos = file.rfind(L'\\');  //position of last slash '\'
	if (pos != Unicode_string::npos) 
	{
		rval = file.substr( 0, pos ).c_str();
	}
	return rval;
}

//---------------------------------------------------------------------------

Filename& Filename::strip_ext( bool strip_period )
{
	Unicode_string::size_type posd = file.rfind('.');  //position of last dot '.'
	if (posd != Unicode_string::npos)
	{
		Unicode_string::size_type poss = file.rfind( '\\' );
		if (poss == Unicode_string::npos || posd > poss) 
		{
			if (!strip_period && posd < file.size() - 1) 
			{
				++posd;
			}
			file.resize( posd );
		}
	}
	return *this;
}

//---------------------------------------------------------------------------

void Filename::remove() const
{
	if (DeleteFileW( file.c_str() ) == 0)
	{
		throw Exception( Unicode_string( L"Error removing file " ) +
			file.c_str() + L":\n" + Operating_system::singlet().get_last_error() );
	}
}

//---------------------------------------------------------------------------

bool public_nbs::operator==( const Filename& f1, const Filename& f2 )
{
	Filename temp1( f1 );
	Filename temp2( f2 );
	if (is_short_name( temp1.c_str() )) 
	{
		temp1 = Filename::make_long_filename( temp1.c_str() );
	}
	if (is_short_name( temp2.c_str() )) 
	{
		temp2 = Filename::make_long_filename( temp2.c_str() );
	}
	Unicode_string temp1a = temp1.c_str();
	Unicode_string temp2a = temp2.c_str();
	to_lower( temp1a );
	to_lower( temp2a );
	return temp1a == temp2a;
}

//---------------------------------------------------------------------------

Filename public_nbs::append_counter( const Filename& file )
{
	Filename rval( file );
	int counter = 10000;
	while (rval.exists() && counter)
	{
		Unicode_string ext = rval.extension();
		Unicode_string body = rval.strip_ext().c_str();
		sint32 i = (sint32)body.size() - 1;
		for (; i >= 0 && body[i] >= L'0' && body[i] <= L'9'; --i) {}
		++i;
		int suffix = to_int_def( body.substr( i ).c_str(), 0 ) + 1;
		body.resize( i );
		rval = body + to_wstr( suffix );
		if (ext.size()) rval.add_ext( ext.c_str() );
		--counter;
	}
	return rval;
}

//---------------------------------------------------------------------------

void public_nbs::copy_file( const Filename& source, const Filename& dest )
{
	if (CopyFileW( source.c_str(), dest.c_str(), false ) == 0)
	{
		throw Exception( Unicode_string( L"Error copying file " ) +
			source.c_str() + L" to " + dest.c_str() + L":\n" +
			Operating_system::singlet().get_last_error() );
	}
}

//---------------------------------------------------------------------------
















// DeviceType.cpp : Implementation of CDeviceType

#include "stdafx.h"
#include "DeviceType.h"
#include "com_utilities.h"
#include "PCLLibrary.h"
#include "Device.h"
using namespace public_nbs;
using namespace public_nbs::pcl_extension;


//---------------------------------------------------------------------------
// CDeviceType
//---------------------------------------------------------------------------

/// ------- Template TODO -------
/// Define your type name here
const wchar_t* const CDeviceType::TYPE_NAME = L"device";

const std::wstring CDeviceType::complete_type_name_( 
	std::wstring( CPCLLibrary::NAMESPACE_NAME ) + L"::" + CDeviceType::TYPE_NAME );

//---------------------------------------------------------------------------

CDeviceType::CDeviceType()
{
}

//---------------------------------------------------------------------------

STDMETHODIMP CDeviceType::getName(BSTR * name)
{
	COM_METHOD_START

	*name = SysAllocString( TYPE_NAME );

	COM_METHOD_END( L"CDeviceType::getName" ) 
}

//---------------------------------------------------------------------------

STDMETHODIMP CDeviceType::getParentName(BSTR * parent)
{
	COM_METHOD_START

	/// ------- Template TODO -------
	/// If your type has a parent type, return it here.
	/// (Note that you will need to make method call dispatch changes
	/// to other code in this template to support inherited types.)
	*parent = SysAllocString( L""  );

	COM_METHOD_END( L"CDeviceType::getParentName" ) 
}

//---------------------------------------------------------------------------

STDMETHODIMP CDeviceType::getMethods(IPCLMethodList * list)
{
	COM_METHOD_START

	list->AddRef();
	COM_ptr<IPCLMethodList> temp( list );
	Method_list mlist( temp );

	CDevice::operators_new_.clear();
	/// ------- Template TODO -------
	/// add information about any new operators for the type

	CDevice::methods_.clear();
	/// ------- Template TODO -------
	/// add information about the methods of this type
	{
		// add the method which implements this to a local list
		CDevice::methods_.push_back( &CDevice::handle);
		// tell Presentation about the method
		Method m = mlist.add_method();
		m.set_name( L"handle" );
		m.set_description( L"Returns the HHD of the device" );
		m.set_return_value( INT_NAME, 0, L"The HHD of the device" );
	}
	{
		CDevice::methods_.push_back(&CDevice::begin_frame);
		Method m = mlist.add_method();
		m.set_name(L"begin_frame");
		m.set_description(L"Calls hdBeginFrame");
	}
	{
		CDevice::methods_.push_back(&CDevice::end_frame);
		Method m = mlist.add_method();
		m.set_name(L"end_frame");
		m.set_description(L"Calls hdEndFrame");
	}
	{
		CDevice::methods_.push_back(&CDevice::get_current_position);
		Method m = mlist.add_method();
		m.set_name(L"get_current_position");
		m.set_description(L"Calls hdGetDoublev(HD_CURRENT_POSITION)");
		m.add_argument(L"position", DOUBLE_NAME, 1, false, L"Receives the position");
	}
	{
		CDevice::methods_.push_back(&CDevice::get_current_velocity);
		Method m = mlist.add_method();
		m.set_name(L"get_current_velocity");
		m.set_description(L"Calls hdGetDoublev(HD_CURRENT_VELOCITY)");
		m.add_argument(L"velocity", DOUBLE_NAME, 1, false, L"Receives the velocity");
	}
	{
		CDevice::methods_.push_back(&CDevice::get_current_angular_velocity);
		Method m = mlist.add_method();
		m.set_name(L"get_current_angular_velocity");
		m.set_description(L"Calls hdGetDoublev(HD_CURRENT_ANGULAR_VELOCITY)");
		m.add_argument(L"velocity", DOUBLE_NAME, 1, false, L"Receives the angular velocity");
	}
	{
		CDevice::methods_.push_back(&CDevice::get_current_joint_angles);
		Method m = mlist.add_method();
		m.set_name(L"get_current_joint_angles");
		m.set_description(L"Calls hdGetDoublev(HD_CURRENT_JOINT_ANGLES)");
		m.add_argument(L"angles", DOUBLE_NAME, 1, false, L"Receives the angles");
	}
	{
		CDevice::methods_.push_back(&CDevice::get_current_gimbal_angles);
		Method m = mlist.add_method();
		m.set_name(L"get_current_gimbal_angles");
		m.set_description(L"Calls hdGetDoublev(HD_CURRENT_JOINT_ANGLES)");
		m.add_argument(L"angles", DOUBLE_NAME, 1, false, L"Receives the angles");
	}
	{
		CDevice::methods_.push_back(&CDevice::get_current_buttons);
		Method m = mlist.add_method();
		m.set_name(L"get_current_buttons");
		m.set_description(L"Calls hdGetIntegerv(HD_CURRENT_BUTTONS)");
		m.set_return_value(INT_NAME, 0, L"Button bitset");
	}

	COM_METHOD_END( L"CDeviceType::getMethods" ) 
}

//---------------------------------------------------------------------------

STDMETHODIMP CDeviceType::createObject(long index, IPCLArguments* arguments, IPCLObject* * object)
{
	COM_METHOD_START

	if ( (index < 0) || (static_cast<size_t>( index ) >= CDevice::operators_new_.size()) )
	{
		throw public_nbs::Exception( L"Invalid index sent to CDeviceType::createObject" );
	}

	CDevice* temp = 0;
	COM_ptr<IPCLObject> temp_com = create_com_object<IPCLObject>( temp );

	temp->type_.reset( this );
	AddRef();

	public_nbs::COM_ptr<IPCLArguments> args( arguments );
	args->AddRef();
	(temp->*(CDevice::operators_new_[index]))( args );

	temp_com->AddRef();
	*object = temp_com.get();


	COM_METHOD_END( L"CDeviceType::createObject" ) 
}

//---------------------------------------------------------------------------

STDMETHODIMP CDeviceType::getLibrary(IPCLLibrary* * lib)
{
	COM_METHOD_START

	*lib = library_.get();
	library_->AddRef();

	COM_METHOD_END( L"CDeviceType::getLibrary" ) 
}

//---------------------------------------------------------------------------

STDMETHODIMP CDeviceType::getLastError(BSTR * message)
{
	COM_METHOD_START

	*message = SysAllocString( last_error_.c_str() );

	COM_METHOD_END( L"CDeviceType::getLastError" ) 
}

//---------------------------------------------------------------------------

STDMETHODIMP CDeviceType::getDescription(BSTR * desc)
{
	COM_METHOD_START

	/// ------- Template TODO -------
	/// Define a description of your type here
	*desc = SysAllocString( L"Represents an initialized device"  );

	COM_METHOD_END( L"CDeviceType::getDescription" ) 
}

//---------------------------------------------------------------------------
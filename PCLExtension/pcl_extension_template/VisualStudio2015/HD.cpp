// HD.cpp : Implementation of CHD

#include "stdafx.h"
#include "HD.h"
#include "com_utilities.h"
#include "Device.h"
using namespace public_nbs;
using namespace public_nbs::pcl_extension;

//---------------------------------------------------------------------------
// CHD
//---------------------------------------------------------------------------

std::vector<CHD::Method_call> CHD::methods_;
std::vector<CHD::Method_call> CHD::operators_new_;

//---------------------------------------------------------------------------

/// ------- Template TODO -------
/// Add default initialization of data members here
CHD::CHD()
	:
	global_state_( new Global_state )
{
}

//---------------------------------------------------------------------------

STDMETHODIMP CHD::callMethod(long methodIndex, IPCLArguments * arguments)
{
	COM_METHOD_START

	if ( (methodIndex < 0) || (static_cast<size_t>( methodIndex ) >= methods_.size()) )
	{
		throw public_nbs::Exception( L"Invalid index sent to CHD::callMethod" );
	}
	arguments->AddRef();
	public_nbs::COM_ptr<IPCLArguments> temp( arguments );
	(this->*(methods_[methodIndex]))( temp );
	
	COM_METHOD_END( L"CHD::callMethod" );
}

//---------------------------------------------------------------------------
	
STDMETHODIMP CHD::getType(IPCLType* * type)
{
	COM_METHOD_START

	*type = type_.get();
	type_->AddRef();

	COM_METHOD_END( L"CHD::getType" );
}

//---------------------------------------------------------------------------

STDMETHODIMP CHD::getLastError(BSTR * message)
{
	COM_METHOD_START

	*message = SysAllocString( last_error_.c_str() );

	COM_METHOD_END( L"CHD::getLastError" );
}

//---------------------------------------------------------------------------
/// ------- Template TODO -------
/// Implement your class members here
//---------------------------------------------------------------------------

void CHD::init_api( public_nbs::pcl_extension::Arguments args )
{
}

//---------------------------------------------------------------------------

void CHD::init_default_device( public_nbs::pcl_extension::Arguments args )
{
	check_arg_count(args.count(), 0, L"CHD::init_default_device");
	auto rval = args.cast_return_value<PCLExtensionObject>(L"CHD::init_default_device");

	CDevice* temp = 0;
	COM_ptr<IPCLObject> temp_com = create_com_object<IPCLObject>(temp);
	temp->init(global_state_);

	rval->set_value(temp_com);
}

//---------------------------------------------------------------------------

void CHD::init_device( public_nbs::pcl_extension::Arguments args )
{
	check_arg_count(args.count(), 1, L"CHD::init_device");
	auto config_name = args.cast_argument<PCLString>(0, L"CHD::init_device");
	auto rval = args.cast_return_value<PCLExtensionObject>(L"CHD::init_device");

	CDevice* temp = 0;
	COM_ptr<IPCLObject> temp_com = create_com_object<IPCLObject>(temp);
	temp->init(global_state_, unicode_to_single_byte(config_name->value()).c_str());

	rval->set_value(temp_com);
}

//---------------------------------------------------------------------------

void CHD::get_error_string( public_nbs::pcl_extension::Arguments args )
{
	check_arg_count( args.count(), 1, L"CHD::get_error_string" );
	auto error_code = args.cast_argument<PCLInt>(0, L"CHD::get_error_string");
	auto rval = args.cast_return_value<PCLString>(L"CHD::get_error_string");

	rval->set_value(single_byte_to_unicode(hdGetErrorString(error_code->value())));
}

//---------------------------------------------------------------------------

void CHD::get_error( public_nbs::pcl_extension::Arguments args )
{
	check_arg_count( args.count(), 4, L"CHD::get_error" );
	auto error_code = args.cast_argument<PCLInt>(0, L"CHD::get_error");
	auto description = args.cast_argument<PCLString>(1, L"CHD::get_error");
	auto internal_error_code = args.cast_argument<PCLInt>(2, L"CHD::get_error");
	auto handle = args.cast_argument<PCLInt>(3, L"CHD::get_error");
	auto rval = args.cast_return_value<PCLBool>(L"CHD::get_error");

	HDErrorInfo error = hdGetError();
	rval->set_value(HD_DEVICE_ERROR(error));
	if (HD_DEVICE_ERROR(error))
	{
		error_code->set_value(error.errorCode);
		description->set_value(single_byte_to_unicode(hdGetErrorString(error.errorCode)));
		internal_error_code->set_value(error.internalErrorCode);
		handle->set_value(error.hHD);
	}
}

//---------------------------------------------------------------------------
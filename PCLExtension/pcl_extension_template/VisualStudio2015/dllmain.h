// dllmain.h : Declaration of module class.

class Cpcl_extension_templateModule : public CAtlDllModuleT< Cpcl_extension_templateModule >
{
public :
	DECLARE_LIBID(LIBID_pcl_extension_templateLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_PCL_EXTENSION_TEMPLATE, "{7CC88039-583D-44D8-A02D-05BF755FA40A}")
};

extern class Cpcl_extension_templateModule _AtlModule;

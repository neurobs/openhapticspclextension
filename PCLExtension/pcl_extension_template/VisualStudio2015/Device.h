// Device.h : Declaration of the CDevice

#pragma once
#include "resource.h"       // main symbols

#include "pcl_extension_template_i.h"
#include "PCLExtension.h"
#include "pcl_extension_interface_wrappers.h"
#include <vector>
#include "HD.h"
#include <HD/hd.h>

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif



// CDevice

/// ------- Template Notes -------
/// This is an example IPCLObject implementation which you may reuse.
/// It functions in conjuction with the CDeviceType class
class ATL_NO_VTABLE CDevice :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CDevice, &CLSID_Device>,
	public IDispatchImpl<IPCLObject, &__uuidof(IPCLObject), &LIBID_PCLExtension>
{
public:
	CDevice();
	~CDevice();

	DECLARE_REGISTRY_RESOURCEID(IDR_DEVICE)

	BEGIN_COM_MAP(CDevice)
		COM_INTERFACE_ENTRY(IPCLObject)
		COM_INTERFACE_ENTRY(IDispatch)
	END_COM_MAP()

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:
	// IPCLObject Methods
	STDMETHOD(callMethod)(long methodIndex, IPCLArguments * arguments);
	STDMETHOD(getType)(IPCLType* * type);
	STDMETHOD(getLastError)(BSTR * message);

	public_nbs::COM_ptr<IPCLType> type_;

	// public methods used by others
	void init(std::shared_ptr<Global_state> global_state, std::string config_name = "");

private:
	std::wstring last_error_;

	typedef void (CDevice::*Method_call)( public_nbs::pcl_extension::Arguments args );
	static std::vector<Method_call> methods_;
	static std::vector<Method_call> operators_new_;
	friend class CDeviceType;

	void get_double_data(public_nbs::pcl_extension::Arguments args, HDenum type, unsigned int size);

	/// ------- Template TODO -------
	/// Add your method or operator new initialization method 
	/// implementations for this type here
	void handle( public_nbs::pcl_extension::Arguments args );
	void begin_frame(public_nbs::pcl_extension::Arguments args);
	void end_frame(public_nbs::pcl_extension::Arguments args);
	void get_current_position(public_nbs::pcl_extension::Arguments args);
	void get_current_velocity(public_nbs::pcl_extension::Arguments args);
	void get_current_angular_velocity(public_nbs::pcl_extension::Arguments args);
	void get_current_joint_angles(public_nbs::pcl_extension::Arguments args);
	void get_current_gimbal_angles(public_nbs::pcl_extension::Arguments args);
	void get_current_buttons(public_nbs::pcl_extension::Arguments args);
	
	/// ------- Template TODO -------
	/// Add your object data here
	std::shared_ptr<Global_state> global_state_;
	HHD handle_;
};

OBJECT_ENTRY_AUTO(__uuidof(Device), CDevice)

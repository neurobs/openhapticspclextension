// HDType.cpp : Implementation of CHDType

#include "stdafx.h"
#include "HDType.h"
#include "com_utilities.h"
#include "PCLLibrary.h"
#include "HD.h"
#include "DeviceType.h"
using namespace public_nbs;
using namespace public_nbs::pcl_extension;


//---------------------------------------------------------------------------
// CHDType
//---------------------------------------------------------------------------

/// ------- Template TODO -------
/// Define your type name here
const wchar_t* const CHDType::TYPE_NAME = L"hd";

const std::wstring CHDType::complete_type_name_( 
	std::wstring( CPCLLibrary::NAMESPACE_NAME ) + L"::" + CHDType::TYPE_NAME );

//---------------------------------------------------------------------------

CHDType::CHDType()
{
}

//---------------------------------------------------------------------------

STDMETHODIMP CHDType::getName(BSTR * name)
{
	COM_METHOD_START

	*name = SysAllocString( TYPE_NAME );

	COM_METHOD_END( L"CHDType::getName" ) 
}

//---------------------------------------------------------------------------

STDMETHODIMP CHDType::getParentName(BSTR * parent)
{
	COM_METHOD_START

	/// ------- Template TODO -------
	/// If your type has a parent type, return it here.
	/// (Note that you will need to make method call dispatch changes
	/// to other code in this template to support inherited types.)
	*parent = SysAllocString( L""  );

	COM_METHOD_END( L"CHDType::getParentName" ) 
}

//---------------------------------------------------------------------------

STDMETHODIMP CHDType::getMethods(IPCLMethodList * list)
{
	COM_METHOD_START

	list->AddRef();
	COM_ptr<IPCLMethodList> temp( list );
	Method_list mlist( temp );

	CHD::operators_new_.clear();
	/// ------- Template TODO -------
	/// add information about any new operators for the type
	{
		// add the initialization method which implements this to a local list
		CHD::operators_new_.push_back( &CHD::init_api);
		// tell Presentation about the method
		Method m = mlist.add_new();
		m.set_description( L"Creates an hd object" );
	}
	
	CHD::methods_.clear();
	/// ------- Template TODO -------
	/// add information about the methods of this type
	{
		// add the method which implements this to a local list
		CHD::methods_.push_back( &CHD::init_default_device);
		// tell Presentation about the method
		Method m = mlist.add_method();
		m.set_name( L"init_default_device" );
		m.set_description( L"Calls hdInitDevice(HD_DEFAULT_DEVICE) and returns an object representing that device" );
		m.set_return_value(CDeviceType::complete_type_name_, 0, L"The new object");
	}
	{
		CHD::methods_.push_back( &CHD::init_device);
		Method m = mlist.add_method();
		m.set_name( L"init_device" );
		m.set_description( L"Calls hdInitDevice() and returns an object representing that device" );
		m.add_argument( L"config_name", STRING_NAME, 0, false, L"Name passed to hdInitDevice" );
		m.set_return_value(CDeviceType::complete_type_name_, 0, L"The new object");
	}
	{
		CHD::methods_.push_back( &CHD::get_error_string);
		Method m = mlist.add_method();
		m.set_name( L"get_error_string" );
		m.set_description( L"Calls hdGetErrorString" );
		m.add_argument( L"error_code", INT_NAME, 0, false, L"Error code for which to get the description" );
		m.set_return_value(STRING_NAME, 0, L"The error description");
	}
	{
		CHD::methods_.push_back( &CHD::get_error);
		Method m = mlist.add_method();
		m.set_name( L"get_error" );
		m.set_description( L"Calls hdGetError" );
		m.add_argument(L"error_code", INT_NAME, 0, true, L"Filled with the value of HDErrorInfo::errorCode");
		m.add_argument(L"error_code_description", STRING_NAME, 0, true, L"Filled with the results of hdGetErrorString for the error_code value");
		m.add_argument(L"internal_error_code", INT_NAME, 0, true, L"Filled with the value of HDErrorInfo::internalErrorCode");
		m.add_argument(L"device_handle", INT_NAME, 0, true, L"Filled with the value of HDErrorInfo::hHD");
		m.set_return_value( BOOL_NAME, 0, L"Whether there was an error available" );
	}

	COM_METHOD_END( L"CHDType::getMethods" ) 
}

//---------------------------------------------------------------------------

STDMETHODIMP CHDType::createObject(long index, IPCLArguments* arguments, IPCLObject* * object)
{
	COM_METHOD_START

	if ( (index < 0) || (static_cast<size_t>( index ) >= CHD::operators_new_.size()) )
	{
		throw public_nbs::Exception( L"Invalid index sent to CHDType::createObject" );
	}

	CHD* temp = 0;
	COM_ptr<IPCLObject> temp_com = create_com_object<IPCLObject>( temp );

	temp->type_.reset( this );
	AddRef();

	public_nbs::COM_ptr<IPCLArguments> args( arguments );
	args->AddRef();
	(temp->*(CHD::operators_new_[index]))( args );

	temp_com->AddRef();
	*object = temp_com.get();

	COM_METHOD_END( L"CHDType::createObject" ) 
}

//---------------------------------------------------------------------------

STDMETHODIMP CHDType::getLibrary(IPCLLibrary* * lib)
{
	COM_METHOD_START

	*lib = library_.get();
	library_->AddRef();

	COM_METHOD_END( L"CHDType::getLibrary" ) 
}

//---------------------------------------------------------------------------

STDMETHODIMP CHDType::getLastError(BSTR * message)
{
	COM_METHOD_START

	*message = SysAllocString( last_error_.c_str() );

	COM_METHOD_END( L"CHDType::getLastError" ) 
}

//---------------------------------------------------------------------------

STDMETHODIMP CHDType::getDescription(BSTR * desc)
{
	COM_METHOD_START

	/// ------- Template TODO -------
	/// Define a description of your type here
	*desc = SysAllocString( L"Provides access to the OpenHaptics Toolkit hd functions"  );

	COM_METHOD_END( L"CHDType::getDescription" ) 
}

//---------------------------------------------------------------------------
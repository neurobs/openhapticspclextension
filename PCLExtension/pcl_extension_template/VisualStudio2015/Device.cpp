// Device.cpp : Implementation of CDevice

#include "stdafx.h"
#include "Device.h"
#include "com_utilities.h"
using namespace public_nbs;
using namespace public_nbs::pcl_extension;

//---------------------------------------------------------------------------
// CDevice
//---------------------------------------------------------------------------

std::vector<CDevice::Method_call> CDevice::methods_;
std::vector<CDevice::Method_call> CDevice::operators_new_;

//---------------------------------------------------------------------------

/// ------- Template TODO -------
/// Add default initialization of data members here
CDevice::CDevice() : handle_( -1 )
{
}

//---------------------------------------------------------------------------

CDevice::~CDevice()
{
	if (handle_ != -1)
	{
		hdDisableDevice(handle_);
		global_state_->device_was_disabled(handle_);
	}
}

//---------------------------------------------------------------------------

void CDevice::init(std::shared_ptr<Global_state> global_state, std::string config_name)
{
	global_state_ = global_state;
	auto temp = hdInitDevice(config_name.c_str());

	HDErrorInfo error = hdGetError();
	if (HD_DEVICE_ERROR(error)) {
		throw Exception(single_byte_to_unicode(hdGetErrorString(error.errorCode)));
	}

	handle_ = temp;
	global_state_->device_was_initialized(handle_);
}

//---------------------------------------------------------------------------

STDMETHODIMP CDevice::callMethod(long methodIndex, IPCLArguments * arguments)
{
	COM_METHOD_START

	if ( (methodIndex < 0) || (static_cast<size_t>( methodIndex ) >= methods_.size()) )
	{
		throw public_nbs::Exception( L"Invalid index sent to CDevice::callMethod" );
	}
	arguments->AddRef();
	public_nbs::COM_ptr<IPCLArguments> temp( arguments );
	(this->*(methods_[methodIndex]))( temp );
	
	COM_METHOD_END( L"CDevice::callMethod" );
}

//---------------------------------------------------------------------------
	
STDMETHODIMP CDevice::getType(IPCLType* * type)
{
	COM_METHOD_START

	*type = type_.get();
	type_->AddRef();

	COM_METHOD_END( L"CDevice::getType" );
}

//---------------------------------------------------------------------------

STDMETHODIMP CDevice::getLastError(BSTR * message)
{
	COM_METHOD_START

	*message = SysAllocString( last_error_.c_str() );

	COM_METHOD_END( L"CDevice::getLastError" );
}

//---------------------------------------------------------------------------
/// ------- Template TODO -------
/// Implement your class members here
//---------------------------------------------------------------------------

void CDevice::handle( public_nbs::pcl_extension::Arguments args )
{
	auto rval = args.cast_return_value<PCLInt>(L"CDevice::handle");

	rval->set_value(handle_);
}

//---------------------------------------------------------------------------

void CDevice::begin_frame(public_nbs::pcl_extension::Arguments args)
{
	hdBeginFrame(handle_);
}

//---------------------------------------------------------------------------

void CDevice::end_frame(public_nbs::pcl_extension::Arguments args)
{
	hdEndFrame(handle_);
}

//---------------------------------------------------------------------------

void CDevice::get_double_data(public_nbs::pcl_extension::Arguments args, HDenum type, unsigned int size)
{
	global_state_->verify_current_device(handle_);

	check_arg_count(args.count(), 1, L"CDevice::get_double_data");
	auto data = args.cast_argument<PCLDoubleArray>(0, L"CDevice::get_double_data");
	data->resize(size);
	hdGetDoublev(type, data->get_data_ptr());
}

//---------------------------------------------------------------------------

void CDevice::get_current_position(public_nbs::pcl_extension::Arguments args)
{
	get_double_data(args, HD_CURRENT_POSITION, 3);
}

//---------------------------------------------------------------------------

void CDevice::get_current_velocity(public_nbs::pcl_extension::Arguments args)
{
	get_double_data(args, HD_CURRENT_VELOCITY, 3);
}

//---------------------------------------------------------------------------

void CDevice::get_current_angular_velocity(public_nbs::pcl_extension::Arguments args)
{
	get_double_data(args, HD_CURRENT_ANGULAR_VELOCITY, 3);
}

//---------------------------------------------------------------------------

void CDevice::get_current_joint_angles(public_nbs::pcl_extension::Arguments args)
{
	get_double_data(args, HD_CURRENT_JOINT_ANGLES, 3);
}

//---------------------------------------------------------------------------

void CDevice::get_current_gimbal_angles(public_nbs::pcl_extension::Arguments args)
{
	get_double_data(args, HD_CURRENT_GIMBAL_ANGLES, 3);
}

//---------------------------------------------------------------------------

void CDevice::get_current_buttons(public_nbs::pcl_extension::Arguments args)
{
	global_state_->verify_current_device(handle_);

	auto rval = args.cast_return_value<PCLInt>(L"CDevice::get_current_buttons");
	
	HDint temp;
	hdGetIntegerv(HD_CURRENT_BUTTONS, &temp);
	rval->set_value(temp);
}

//---------------------------------------------------------------------------
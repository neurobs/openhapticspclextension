// HD.h : Declaration of the CHD

#pragma once
#include "resource.h"       // main symbols

#include "pcl_extension_template_i.h"
#include "PCLExtension.h"
#include "pcl_extension_interface_wrappers.h"
#include <vector>
#include <HD/hd.h>

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

class Global_state
{
public:
	void verify_current_device(HHD device)
	{
		if (current_device_ != device)
		{
			hdMakeCurrentDevice(device);
			current_device_ = device;
		}
	}
	void device_was_initialized(HHD device)
	{
		current_device_ = device;
	}
	void device_was_disabled(HHD device)
	{
		if (current_device_ == device)
		{
			current_device_ = INVALID_HDD_VALUE;
		}
	}

private:
	static const HHD INVALID_HDD_VALUE = -1;
	HHD current_device_ = INVALID_HDD_VALUE;
};

// CHD

/// ------- Template Notes -------
/// This is an example IPCLObject implementation which you may reuse.
/// It functions in conjuction with the CHDType class
class ATL_NO_VTABLE CHD :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CHD, &CLSID_HD>,
	public IDispatchImpl<IPCLObject, &__uuidof(IPCLObject), &LIBID_PCLExtension>
{
public:
	CHD();

	DECLARE_REGISTRY_RESOURCEID(IDR_HD)

	BEGIN_COM_MAP(CHD)
		COM_INTERFACE_ENTRY(IPCLObject)
		COM_INTERFACE_ENTRY(IDispatch)
	END_COM_MAP()

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:
	// IPCLObject Methods
	STDMETHOD(callMethod)(long methodIndex, IPCLArguments * arguments);
	STDMETHOD(getType)(IPCLType* * type);
	STDMETHOD(getLastError)(BSTR * message);

	public_nbs::COM_ptr<IPCLType> type_;

private:
	std::wstring last_error_;

	typedef void (CHD::*Method_call)( public_nbs::pcl_extension::Arguments args );
	static std::vector<Method_call> methods_;
	static std::vector<Method_call> operators_new_;
	friend class CHDType;

	/// ------- Template TODO -------
	/// Add your method or operator new initialization method 
	/// implementations for this type here
	void init_api(public_nbs::pcl_extension::Arguments args);
	void init_default_device(public_nbs::pcl_extension::Arguments args);
	void init_device(public_nbs::pcl_extension::Arguments args);
	void get_error_string(public_nbs::pcl_extension::Arguments args);
	void get_error(public_nbs::pcl_extension::Arguments args);
	
	/// ------- Template TODO -------
	/// Add your object data here
	std::shared_ptr<Global_state> global_state_;
};

OBJECT_ENTRY_AUTO(__uuidof(HD), CHD)

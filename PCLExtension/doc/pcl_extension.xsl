<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<xsl:template match="author"/>
<xsl:template match="description"/>
<xsl:template match="name"/>
<xsl:template match="namespace_name"/>
<xsl:template match="parent_name"/>
<xsl:template match="version"/>
<xsl:template match="version_requirement"/>
<xsl:template match="type"/>
<xsl:template match="doc_file"/>
<xsl:template match="return_type"/>
<xsl:template match="return_value_description"/>
<xsl:template match="doc_file"/>
<xsl:template match="pcl_extension_name"/>
<xsl:template match="signature"/>

<xsl:template match="pcl_extensions">
	<html>
	<head>
	<link rel="stylesheet" type="text/css" href="pcl_extension.css" />
	</head>
	<body>
	<xsl:apply-templates/>
	</body>
	</html>
</xsl:template>

<xsl:template match="pcl_extension_types">
	<html>
	<head>
	<link rel="stylesheet" type="text/css" href="pcl_extension.css" />
	</head>
	<body>
	<xsl:apply-templates/>
	</body>
	</html>
</xsl:template>

<xsl:template match="pcl_extension_type">
	<center><h1><xsl:value-of select="name"/></h1></center>
	<table class="library_table">
	<tr>
	<td><b>Description:</b></td>
	<td><xsl:value-of select="description"/></td>
	</tr>
	<tr>
	<td><b>Library:</b></td>
	<td>
	<xsl:element name="a">
		<xsl:attribute name="href"><xsl:value-of select="pcl_extension_name"/>.xml</xsl:attribute><xsl:value-of select="pcl_extension_name"/>
	</xsl:element>
	</td>
	</tr>
	<xsl:if test="parent_name != ''">
	<tr>
	<td><b>Parent:</b></td>
	<td><xsl:value-of select="parent_name"/></td>
	</tr>
	</xsl:if>
	</table>
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="methods">
	<h2>Methods</h2>
	<table class="method_table" border="1">
	<xsl:apply-templates/>
	</table>
</xsl:template>

<xsl:template match="news">
	<h2>new operators</h2>
	<table class="method_table" border="1">
	<xsl:apply-templates/>
	</table>
</xsl:template>

<xsl:template match="method">
	<tr class="signature_row"><td><pre><xsl:value-of select="signature"/></pre></td></tr>
	<tr class="description_row"><td>
	<p><xsl:value-of select="description"/></p>
	<xsl:apply-templates/>
	<xsl:if test="return_value_description != ''">
	<p><b>Returns:</b> <xsl:value-of select="return_value_description"/></p>
	</xsl:if>
	</td></tr>
</xsl:template>

<xsl:template match="arguments">
	<xsl:if test="argument">
	<table class="argument_table" border="1">
	<tr>
	<td><b>Argument</b></td>
	<td><b>Type</b></td>
	<td><b>Description</b></td>
	</tr>
	<xsl:apply-templates/>
	</table>
	</xsl:if>
</xsl:template>

<xsl:template match="argument">
	<tr>
	<td><xsl:value-of select="name"/></td>
	<td><xsl:value-of select="type"/></td>
	<td><xsl:value-of select="description"/></td>
	</tr>
</xsl:template>

<xsl:template match="pcl_extension">
	<center><h1><xsl:value-of select="name"/></h1></center>
	<table class="library_table">
	<tr>
	<td><b>Description:</b></td>
	<td><xsl:value-of select="description"/></td>
	</tr>
	<tr>
	<td><b>Author:</b></td>
	<td><xsl:value-of select="author"/></td>
	</tr>
	<tr>
	<td><b>Version:</b></td>
	<td><xsl:value-of select="version"/></td>
	</tr>
	<tr>
	<td><b>Namespace:</b></td>
	<td><xsl:value-of select="namespace_name"/></td>
	</tr>
	<tr>
	<td><b>Presentation Version Requirement:</b></td>
	<td><xsl:value-of select="version_requirement"/></td>
	</tr>
	</table>
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="type_descriptions">
	<h2>Types</h2>
	<table class="type_description_table">
	<tr class="signature_row">
	<td><b>Name</b></td>
	<td><b>Parent</b></td>
	<td><b>Description</b></td>
	</tr>
	<xsl:apply-templates/>
	</table>
</xsl:template>

<xsl:template match="type_description">
	<tr>
	<td>
	<xsl:element name="a">
		<xsl:attribute name="href"><xsl:value-of select="doc_file"/></xsl:attribute><xsl:value-of select="name"/>
	</xsl:element> 
	</td>
	<td><xsl:value-of select="parent_name"/></td>
	<td><xsl:value-of select="description"/></td>
	</tr>
</xsl:template>


<xsl:template match="variables">
	<xsl:if test="predefined_variable">
	<h2>Predefined Variables</h2>
	<table class="predefined_variables_table">
	<tr>
	<td><b>Name</b></td>
	<td><b>Type</b></td>
	</tr>
	<xsl:apply-templates/>
	</table>
	</xsl:if>
</xsl:template>

<xsl:template match="predefined_variable">
	<tr>
	<td><xsl:value-of select="name"/></td>
	<td><xsl:value-of select="type"/></td>
	</tr>
</xsl:template>


</xsl:stylesheet>